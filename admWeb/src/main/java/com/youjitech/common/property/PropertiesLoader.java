package com.youjitech.common.property;

import com.youjitech.common.util.SpringUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * Created By wangbo
 * 2019/2/21
 */
public class PropertiesLoader {

    private static Logger logger = LoggerFactory.getLogger(PropertiesLoader.class);

    private static ResourceLoader resourceLoader = new DefaultResourceLoader();
    private final Properties properties;

    protected static PropertiesLoader propertiesDevLoader = new PropertiesLoader("classpath:/properties/application-dev.properties");
    protected static PropertiesLoader propertiesTestLoader = new PropertiesLoader("classpath:/properties/application-test.properties");
    protected static PropertiesLoader propertiesProdLoader = new PropertiesLoader("classpath:/properties/application-prod.properties");
    //private static String environment = SpringUtils.getBean(Environment.class).getDefaultProfiles()[0];
    private static String environment = SpringUtils.getBean(Environment.class).getActiveProfiles().length>0?SpringUtils.getBean(Environment.class).getActiveProfiles()[0]:SpringUtils.getBean(Environment.class).getDefaultProfiles()[0];

    private final static String ENV_PROD = "prod";
    private final static String ENV_TEST = "test";
    private final static String ENV_DEV = "dev";

    public PropertiesLoader(String ... resourcesPaths) {
        this.properties = loadProperties(resourcesPaths);
    }

    public Properties getProperties() {
        return this.properties;
    }

    private String getValue(String key) {
        String systemProperty = System.getProperty(key);
        if (systemProperty != null) {
            return systemProperty;
        }
        return this.properties.getProperty(key);
    }

    public String getProperty(String key) {
        String value = getValue(key);
        if (value == null) {
            throw new NoSuchElementException();
        }
        return value;
    }

    public String getProperty(String key, String defaultValue) {
        String value = getValue(key);
        return ((value != null) ? value : defaultValue);
    }

    public Integer getInteger(String key) {
        String value = getValue(key);
        if (value == null) {
            throw new NoSuchElementException();
        }
        return Integer.valueOf(value);
    }

    public Integer getInteger(String key, Integer defaultValue) {
        String value = getValue(key);
        return ((value != null) ? Integer.valueOf(value) : defaultValue);
    }

    public Double getDouble(String key) {
        String value = getValue(key);
        if (value == null) {
            throw new NoSuchElementException();
        }
        return Double.valueOf(value);
    }

    public Double getDouble(String key, Integer defaultValue) {
        String value = getValue(key);
        return Double.valueOf(defaultValue.intValue());
    }

    public Boolean getBoolean(String key) {
        String value = getValue(key);
        if (value == null) {
            throw new NoSuchElementException();
        }
        return Boolean.valueOf(value);
    }

    public Boolean getBoolean(String key, boolean defaultValue) {
        String value = getValue(key);
        return Boolean.valueOf((value != null) ? Boolean.valueOf(value)
                .booleanValue() : defaultValue);
    }

    private Properties loadProperties(String[] resourcesPaths) {
        Properties props = new Properties();

        for (String location : resourcesPaths) {
            logger.debug("Loading properties file from path:{}", location);

            InputStream is = null;
            try {
                Resource resource = resourceLoader.getResource(location);
                is = resource.getInputStream();
                props.load(is);
            } catch (IOException ex) {
                logger.info("Could not load properties from path:{}, {} ",
                        location, ex.getMessage());
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
        return props;
    }

    /**
     * 根据实际环境变量获取配置值
     * @param key
     * @return
     */
    public static String getEnvValue(String key) {

        String property = null;

        logger.debug("environment : " + environment);
        try{
            property = propertiesProdLoader.getProperty(key);;
            if(ENV_DEV.equalsIgnoreCase(environment)) {
                if(!StringUtils.isEmpty(propertiesDevLoader.getProperty(key))) {
                    property = propertiesDevLoader.getProperty(key);
                }
            }else if(ENV_TEST.equalsIgnoreCase(environment)) {
                if(!StringUtils.isEmpty(propertiesTestLoader.getProperty(key))) {
                    property = propertiesTestLoader.getProperty(key);
                }
            }
        }catch(Exception e){
            logger.warn("propertiesLoader not exit, key:{}", key, e.getMessage());
        }

        return property;
    }

}
