package com.youjitech.common;

/**
 * Created By wangbo
 * 2019/1/16
 */
public class Constant {

    /**
     * 未登录
     */
    public static final String UNLOGIN_CODE = "00001";

    /**
     * 操作成功
     */
    public static final String SUCCESS_CODE = "10000";

    /**
     * 操作失败
     */
    public static final String FAIL_CODE = "10001";

    /**
     * 系统异常
     */
    public static final String SYSTEM_ERROR_CODE = "10002";

    /**
     * 没有操作权限
     */
    public static final String NO_PERMISSION_CODE = "00002";

    /**
     * 未登录
     */
    public static final String WRONG_LOGIN_NAME_OR_PASSWORD = "00003";

    //用户状态
    public static final String USER_STATUS_NORMAL = "NORMAL";//正常
    public static final String USER_STATUS_FROZEN = "FREEZE";//冻结


    /**
     * 默认可用资源等级 > 1
     */
    public static final int DEFAULT_RESOURCE_AVAILABLE_LEVEL = 1;


    /**
     * 默认可用资源权限 view
     */
    public static final String DEFAULT_BASIC_AVAILABLE_PERMISSION = "view";


    /**
     * 资源等级
     */
    public static final int RESOURCE_LEVEL_ONE = 1;
    public static final int RESOURCE_LEVEL_TWO = 2;
    public static final int RESOURCE_LEVEL_THREE = 3;


    public static final int FLAG_INT_NO = 0;
    public static final int FLAG_INT_YES = 1;

    public static final String FLAG_STRING_NO = "0";
    public static final String FLAG_STRING_YES = "1";

    public final static String MENU_NAME_KEY = "MENU_NAME";

    public final static int DEFALUT_ADDRESS_DEPTH = 0;

    public static final String FLAG_STRING_N = "N";
    public static final String FLAG_STRING_Y = "Y";

    //language
    public final static String LANG_ZH_CN = "zh";
    public final static String LANG_KO = "ko_kr";
    public final static String LANG_EN = "en";


}

