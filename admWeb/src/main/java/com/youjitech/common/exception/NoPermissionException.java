package com.youjitech.common.exception;

/**
 * Created By wangbo
 * 2019/1/16
 */
public class NoPermissionException extends RuntimeException {
    /**
     * Creates a new NoPermissionException.
     */
    public NoPermissionException() {
        super();
    }

    /**
     * Constructs a new NoPermissionException.
     *
     * @param message the reason for the exception
     */
    public NoPermissionException(String message) {
        super(message);
    }

    /**
     * Constructs a new NoPermissionException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public NoPermissionException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new NoPermissionException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public NoPermissionException(String message, Throwable cause) {
        super(message, cause);
    }
}
