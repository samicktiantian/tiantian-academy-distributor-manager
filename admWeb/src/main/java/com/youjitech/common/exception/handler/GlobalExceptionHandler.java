package com.youjitech.common.exception.handler;

import com.youjitech.common.Constant;
import com.youjitech.common.exception.YoujiException;
import com.youjitech.common.util.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerExceptionResolverComposite;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @author wangbo created on ：2017年11月6日 上午9:04:15
 */
@Slf4j
public class GlobalExceptionHandler extends HandlerExceptionResolverComposite {
	
	@Autowired
	private MessageSource messageSource;
	
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

		Locale locale = RequestContextUtils.getLocale(request);
		//System.out.println(ex);
		//判断是否是异步操作
		if(WebUtil.isAjax(request)) {

			ModelAndView modelAndView = null;

			//异步操作
			if(ex instanceof AuthorizationException) {
				log.error("Authorization fail {}", ex.getMessage());
				// 未授权操作
				modelAndView = generateModelAndView(request, "exception.nopermission", null, locale);
			} else if(ex instanceof AuthenticationException) {
				// 账号或者密码错误
				WebUtil.sendJson(response, Constant.WRONG_LOGIN_NAME_OR_PASSWORD, messageSource.getMessage("exception.wrong.loginname.password", null, locale));
				log.error("Authentication fail {}", ex.getMessage());
//				modelAndView.addObject("message", messageSource.getMessage("exception.wrong.loginname.password", null, locale));
			} else if(ex instanceof YoujiException) {
				//自定义操作异常
				log.error("YoujiException fail {}", ex.getMessage());
				modelAndView = generateModelAndView(request, ex.getMessage(), null, locale);
			}else {
				//其他操作异常
				String message = getMessageContent(ex.getMessage(), locale);
				WebUtil.sendJson(response, Constant.SYSTEM_ERROR_CODE, getMessageContent(ex.getMessage(), locale));
				log.error("system error {}", ex.getMessage());
			}
			return modelAndView;
		}else {
			String message = getMessageContent(ex.getMessage(), locale);
			WebUtil.sendJson(response, Constant.SYSTEM_ERROR_CODE, message);
//			resolveException(request, response, handler, ex);
			return null;
		}
	}

	private String getMessageContent(String messageCode, Locale locale){
		String message = null;
		try {
			message =  messageSource.getMessage(messageCode, null, locale);
		} catch (Exception e) {
			message = messageSource.getMessage("exception.data.handle.error", null, locale);
		}
		return message;
	}

	private ModelAndView generateModelAndView(HttpServletRequest request, String code, String message, Locale locale){
		String codeMessage = message;
		if(!StringUtils.isEmpty(code)){
			codeMessage = messageSource.getMessage(code, null, locale);
		}
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("error");
		modelAndView.addObject("msg", codeMessage);
		return modelAndView;
	}
}
