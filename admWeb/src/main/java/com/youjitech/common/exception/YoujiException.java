package com.youjitech.common.exception;

/**
 * Created By wangbo
 * 2019/1/16
 */
public class YoujiException extends RuntimeException {

    private String code;

    public YoujiException(String code, String message) {
        super(message);
        this.code = code;
    }

    public YoujiException(String message) {
        super(message);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
