package com.youjitech.common.util.web.json.data;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.youjitech.common.util.web.json.annotation.Utc2Local;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class DateConvertSerializer extends JsonSerializer<String> {

    private Utc2Local anno;


    public DateConvertSerializer() {
    }

    @Override
    public void serialize(String value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
//        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//        RequestContextUtils.getLocale(request);

        jsonGenerator.writeString(value);
    }

    public void setAnno(Utc2Local anno) {
        this.anno = anno;
    }

}
