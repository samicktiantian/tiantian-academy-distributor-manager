package com.youjitech.common.util.tag;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class CodeCdNameTag extends SimpleTagSupport {
	
	private Integer cdDict;
	private Integer cdCode;
	private String lang;
	
	public void setCdDict(Integer cdDict) {
		this.cdDict = cdDict;
	}

	public void setCdCode(Integer cdCode) {
		this.cdCode = cdCode;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	private Logger _log = LoggerFactory.getLogger(CodeSelectTag.class);

	@Override
	public void doTag() throws JspException, IOException {
		String outText = null;
		
		try {
//			outText = CodeUtil.getCodeName(cdDict, cdCode, lang);
		}catch (Exception e) {
			_log.error("生成标签错误: {}", e.getMessage());
		}
		
		JspWriter out = getJspContext().getOut();
	    out.println(outText);
	}
}
