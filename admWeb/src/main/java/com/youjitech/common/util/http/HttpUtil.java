package com.youjitech.common.util.http;

import com.alibaba.fastjson.JSONObject;
import com.youjitech.common.property.PropertiesLoader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Created By wangbo
 * 2019/3/5
 */
public class HttpUtil {

    private volatile static HttpUtil instance = null;

    private static RestTemplate restTemplate = null;

    private HttpUtil() {
    }

    public static HttpUtil getInstance() {
        if (instance == null) {
            synchronized (HttpUtil.class) {
                if (instance == null) {
                    instance = new HttpUtil();

                    SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
                    factory.setReadTimeout(10000);
                    factory.setConnectTimeout(15000);

                    restTemplate = new RestTemplate(factory);
                }
            }
        }
        return instance;
    }

    public RestTemplate getRestTemplate(){
        return restTemplate;
    }

    public String generateNodeApiUserPasswd(){

        String url = PropertiesLoader.getEnvValue("node.api.url.prefix") + "/v2/notify/genPasswd";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap map = new LinkedMultiValueMap();
        map.add("passwd", "123456");

        HttpEntity requestBody = new HttpEntity(map, headers);

        ResponseEntity<JSONObject> jsonObjectResponseEntity = HttpUtil.getInstance().getRestTemplate().postForEntity(url, requestBody, JSONObject.class);

        JSONObject jsonObject = jsonObjectResponseEntity.getBody();

        return (String)jsonObject.get("data");
    }

    public static void main(String[] args) {


        System.out.println(getInstance().generateNodeApiUserPasswd());
    }
}
