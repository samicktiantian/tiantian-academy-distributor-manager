package com.youjitech.common.util;

import java.util.Random;

/**
 * 
 * 加密工具类
 * @version 1.0
 */
public class EncryptUtil {
	public static char[] codes = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
			'I', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
			'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'i', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v',
			'w', 'x', 'y', 'z', '0','1','2', '3', '4', '5', '6', '7', '8', '9' };

	

	/**
	 * 
	 * 取一个指定最小长度和最大长度的随机码，如图，图形检验码
	 * @param max 最大长度
	 * @param min 最小长度
	 * @return 随机码
	 */
	public static String getRandomPassword(final int max,final int min) {
		final StringBuffer password = new StringBuffer();
		final Random r = new Random();
		final int psdLength = r.nextInt(max-min + 1)+ min;

		for (int i = 0; i < psdLength; i++) {
			password.append(String.valueOf(codes[r.nextInt(codes.length)]));
		}

		return password.toString();
	}
	/**
	 * 
	 * 取一个指定长度的随机码，如图，图形检验码
	 * @return 随机码
	 */
	public static String getRandomString(final int len) {
		return getRandomPassword(len,len);
	}
}
