package com.youjitech.common.util;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.Calendar;
import java.util.Date;

/** 
* @author wangbo 
* created on ：2017年11月1日 下午3:27:39 
*/
public class UploadUtil {
	
	private final static String FILE_UPLOAD_PATH = System.getProperty("java.io.tmpdir"); 
	
	public final static Integer FILE_TYPE_DOCUMENT = 1;
	public final static Integer FILE_TYPE_PICTURE = 2;
	
	public final static String DEFAULT = "default";

	public static String[] getCreatePathWithSuffix(String uploadPath, String realName,String sign){
//		String type = realName.substring(realName.lastIndexOf(".") + 1);
//		String suffix = "";
//		if(!type.isEmpty()){
//			suffix = StringPool.PERIOD + type;
//		}
		String path = FILE_UPLOAD_PATH;
		
		if(!StringUtils.isEmpty(uploadPath)){
			path = uploadPath;
		}
		
//		String name = UUID.randomUUID().toString() + "-" + (int)(Math.random() * 10000) + suffix;
		
		String dateStr = DateUtil.convertToString(new Date(), DateUtil.YMD1) + StringPool.SLASH + Calendar.getInstance().getTimeInMillis();
		
		String relativePath = (sign == null ? DEFAULT : sign) + StringPool.FORWARD_SLASH + dateStr + StringPool.FORWARD_SLASH + realName;
		
		path = path + StringPool.FORWARD_SLASH + relativePath;
		
		File file = new File((new File(path)).getParent());
		file.mkdirs();
		
		return new String[]{path,relativePath};
	}
	
	public static byte[] getBytes(File file) throws IOException {
		 byte[] buffer = null;  
	        try {  
	            FileInputStream fis = new FileInputStream(file);  
	            ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);  
	            byte[] b = new byte[1000];  
	            int n;  
	            while ((n = fis.read(b)) != -1) {  
	                bos.write(b, 0, n);  
	            }  
	            fis.close();  
	            bos.close();  
	            buffer = bos.toByteArray();  
	        } catch (FileNotFoundException e) {  
	            e.printStackTrace();  
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }  
	        return buffer;  
	}
	
}
