package com.youjitech.common.util.web.json.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotation
public @interface Utc2Local {

    public String value() default "";

    public String format() default "yyyy-MM-dd HH:mm:ss";

    public boolean expand() default false;
}
