package com.youjitech.common.util;

import com.youjitech.system.role.realms.ShiroRDbRealm.ShiroUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

@Slf4j
public class SubjectUtil {

    public static ShiroUser getCurrUser(){
        Subject subject = SecurityUtils.getSubject();
        if(subject == null) {
            return null;
        }
        ShiroUser user = (ShiroUser)subject.getPrincipal();
        return user;
    }
}
