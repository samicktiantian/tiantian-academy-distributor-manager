package com.youjitech.common.util.tag;

import com.youjitech.common.Constant;
import com.youjitech.common.util.SpringUtils;
import com.youjitech.system.code.entity.Code;
import com.youjitech.system.code.util.CodeUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

@Slf4j
public class CodeSelectTag extends SimpleTagSupport {

	private String selectIdName;
	private Integer selectedValue;
	private Integer codeDict;
	private Boolean isMultiple = false;
	private String selectWidth = "143";
	private String cssStyle;
	private String dataOptions;
	private Boolean isReadOnly = false;
	private Boolean isDisabled = false;
	private String language = "zh_cn";
	private String defaultDisplayText = "";
	private String defaultTextvalue = "";
	//spring messageResource key
	private String defaultDisplayTextKey = "";

	public void setDefaultTextvalue(String defaultTextvalue) {
		this.defaultTextvalue = defaultTextvalue;
	}

	public void setIsDisabled(Boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public void setIsReadOnly(Boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}

	public void setDataOptions(String dataOption) {
		this.dataOptions = dataOption;
	}

	public void setIsMultiple(Boolean isMultiple) {
		this.isMultiple = isMultiple;
	}

	public void setCodeDict(Integer codeDict) {
		this.codeDict = codeDict;
	}

	public void setSelectWidth(String selectWidth) {
		this.selectWidth = selectWidth;
	}

	public void setSelectIdName(String selectIdName) {
		this.selectIdName = selectIdName;
	}

	public void setSelectedValue(Integer selectedValue) {
		this.selectedValue = selectedValue;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setDefaultDisplayText(String defaultDisplayText) {
		this.defaultDisplayText = defaultDisplayText;
	}

	public void setDefaultDisplayTextKey(String defaultDisplayTextKey) {
		this.defaultDisplayTextKey = defaultDisplayTextKey;
	}

	@Override
	public void doTag() throws JspException, IOException {
		
		StringBuffer sb = new StringBuffer();
		
		
		try {
			
			List<Code> codeList = CodeUtil.getDictList(codeDict);
			
			String multiple = "";
			if(isMultiple){
				multiple = "multiple";
			}
			
			String selectWidthStr = "";
			if(!StringUtils.isEmpty(selectWidth)){
				//样式为空且自定义长度不为空的情况下,按照给定的长度设置
				selectWidthStr = "style='width:"+selectWidth+"px'";
			}
			
			String cssStyleStr = "";
			if(!StringUtils.isEmpty(cssStyle)){
				cssStyleStr = "class='"+cssStyle+"'";
			}
			
			String option = "";
			if(!StringUtils.isEmpty(dataOptions)){
				option = "data-options='"+dataOptions+"'";
			}
			
			sb.append("<select "+cssStyleStr+" "+option+" name='"+selectIdName+"' id='" + selectIdName + "' " 
					+multiple+ " "+selectWidthStr+" " + (isReadOnly ? "readonly" : " ") + (isDisabled ? "disabled" : "") + ">");
			if(defaultDisplayText != null && !"".equals(defaultDisplayText)) {
				sb.append("<option value='"+defaultTextvalue+"'> " + defaultDisplayText + " </option>");
			}else if(defaultDisplayTextKey != null && !"".equals(defaultDisplayTextKey)) {
				MessageSource messageSource = SpringUtils.getBean(MessageSource.class);
				String message = messageSource.getMessage(defaultDisplayTextKey, null, 
						language.equals(Constant.LANG_ZH_CN) ? Locale.CHINA :
							(language.equalsIgnoreCase(Constant.LANG_KO) ? Locale.KOREA : Locale.ENGLISH));
				sb.append("<option value='"+defaultTextvalue+"'> " + message + " </option>");
			}
			for(Code code : codeList){
				String isSeleted = "";
				if(selectedValue != null && selectedValue.intValue() == code.getCdCode().intValue()){
					isSeleted = "selected";
				}
				
				String selectVal = code.getCdName(language);
				
				sb.append("<option "+isSeleted+" value='"+code.getCdCode()+"'>"+selectVal+"</option>");
			}
			sb.append("</select>");
		}catch (Exception e) {
			log.error("生成标签错误: {}", e.getMessage());
		}
		
		JspWriter out = getJspContext().getOut();
	    out.println(sb.toString());
	}

}
