package com.youjitech.common.util;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import java.util.Map;

public class PageUtil {
	
	private final static int DEFAULT_PAGE_NUM = 1;
	private final static int DEFAULT_PAGE_SIZE = 10;

	/**
	 * 根据请求参数初始化分页对象
	 * @param params
	 */
	public static Page<?> initPage(Map<String, Object> params) {
		int page = DEFAULT_PAGE_NUM;
		if(params.get("page") != null){
			page = Integer.valueOf(params.get("page") + "");
		}
		
		int pageSize = DEFAULT_PAGE_SIZE;
		if(params.get("pageSize") != null){
			pageSize = Integer.valueOf(params.get("pageSize") + "");
		}
		
		Page<?> pageObj = PageHelper.startPage(page, pageSize); 
		
		return pageObj;
	}
	
}
