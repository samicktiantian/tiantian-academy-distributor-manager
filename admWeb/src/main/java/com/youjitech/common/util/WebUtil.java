package com.youjitech.common.util;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/16
 */
public class WebUtil {

    private static Logger _log = LoggerFactory.getLogger(WebUtil.class);

    public static boolean isAjax(ServletRequest request) {

        HttpServletRequest httpServletRequest = (HttpServletRequest)request;

        return !StringUtils.isBlank(httpServletRequest.getHeader("x-requested-with")) && httpServletRequest.getHeader("x-requested-with").equals("XMLHttpRequest");
    }

    public static void sendJson(ServletResponse response, String code, String msg) {

        HttpServletResponse httpServletResponse = (HttpServletResponse)response;

        Map<String, Object> result = Maps.newHashMap();
        result.put("code", code);
        result.put("msg", msg);

        Object json = JSONObject.toJSON(result);

        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json; charset=utf-8");
        httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(json.toString());
        } catch (IOException e) {
            _log.error(e.getMessage());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
