package com.youjitech.common.util;

import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Slf4j
public class DateUtil {
	
	public static final String YMD1 = "yyyy-MM-dd";
    public static final String YMD2 = "dd/MM/yy";
    public static final String YMD3 = "yyyyMMdd";
    public static final String YMD4 = "ddMMyyyy";
    public static final String YMD5 = "yyyy-MM";
    public static final String YMD6 = "HH";
    public static final String YMD7 = "yyyy/MM/dd";
    public static final String YMD8 = "HH:mm:ss";
    public static final String YMD_FULL = "yyyy-MM-dd HH:mm:ss";
    public static final String YMD_FULL2 = "yyyyMMddHHmmssSSS";
    
    public static final String YMD_ZH = "yyyy年MM月dd日";


    
    
    /**
	* Convert date type from String to Date
	* @return Date
	*/
    public static String convertToString(Date date,String format) {
    	if(date==null) return "";
    	SimpleDateFormat sdf=new SimpleDateFormat(format);
    	return sdf.format(date);
    }
    
    public static String convertToString(Date date) {
		return convertToString(date,YMD_FULL);
	}
    
    /**
     * 增加年份
     * @param currentDate
     * @param year 偏移量
     * @return
     */
    public static Date seekYear(Date currentDate, int year){
    	Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + year);
		return calendar.getTime();
    }
    
    /**
     * 增加月份
     * @param currentDate
     * @param month 偏移量
     * @return
     */
    public static Date seekMonth(Date currentDate, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.set(Calendar.MONTH, calendar.get(Calendar.DAY_OF_MONTH) + month);
		return calendar.getTime();
	}
    
    /**
     * 加减天数
     * @param date
     * @param dayNum
     * @return
     */
    public static Date seekDate(Date date,int dayNum) {
		return  new Date(date.getTime() + (long)(dayNum*86400000));
	}
    
    /**
     * 当前年份传 0，去年传-1 ，明年传1
     * @param yearCount
     * @return
     */
    public static Date getFirstDateOfYear(int yearCount){
    	Calendar c = Calendar.getInstance();
//    	c.set(Calendar.YEAR, currentYear);
    	c.add(Calendar.YEAR, yearCount);
    	c.set(Calendar.MONTH, 0);
    	c.set(Calendar.DAY_OF_MONTH, 1);
    	c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
    	return c.getTime();
    }
    
    /**
     * 传0为当月,上一月 -1 下一个 1,以此类推
     * @param month
     * @return
     */
    public static Date getFirstDateOfMonth(int month){
    	Calendar c = Calendar.getInstance();
    	c.set(Calendar.DAY_OF_MONTH, 1);
    	c.set(Calendar.MONTH, c.get(Calendar.MONTH) + month);
    	return c.getTime();
    }
    
    /**
     * 根据日期获取年份
     * @param date
     * @return
     */
    public static int getYear(Date date){
    	Calendar rightNow = Calendar.getInstance();
    	rightNow.setTime(date);
    	return rightNow.get(Calendar.YEAR);
    }
    
    /**
     * 根据日期获取月份
     * @param date
     * @return
     */
    public static int getMonth(Date date){
    	Calendar rightNow = Calendar.getInstance();
    	rightNow.setTime(date);
    	return rightNow.get(Calendar.MONTH) + 1;
    }
    
    public static Date trimDate(Date date) {
    	Calendar rightNow = Calendar.getInstance();
    	rightNow.setTime(date);
    	
		rightNow.set(Calendar.HOUR_OF_DAY, 0);
		rightNow.set(Calendar.MINUTE, 0);
		rightNow.set(Calendar.SECOND, 0);
		
    	return  rightNow.getTime();
	}
    
    public static Date getCurrentDate() {
    	return new Date();
    }
    public static Long getCurrentTime() {
    	return new Date().getTime();
    }
    
    public static int getAge(String birth,String format) {
    	if(birth == null){
    		return 0;
    	}
    	try {
    		return getAge(convertFromString(birth,format));
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
    	
    }
    public static int getAge(Date birth) {
    	if(birth == null){
    		return 0;
    	}
    	Calendar rightNow = Calendar.getInstance();
    	int nowYear = rightNow.get(Calendar.YEAR);
    	rightNow.setTime(birth);
    	int birthYear = rightNow.get(Calendar.YEAR);
    	
    	return nowYear - birthYear;
    	
    }
    
    public static DateFormat getCnDateFormat(String pattern){
		return new SimpleDateFormat(pattern);
	}
    
    public static Date convertFromString(String date,String format) throws Exception{
		if(date==null) return null;
		if(format == null) format = YMD_FULL;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(date);
	}
    
    public static Date convertFromString(String date) throws Exception{
    	return convertFromString(date, null);
    }
    public static Date convertDate(String date) throws Exception{
    	return convertFromString(date, YMD1);
    }
    public static Date convertDateMutip(String date) throws Exception{
    	if(null == date) return null;
    	if(date.length() > 10){
    		return convertFromString(date);
    	}else{
    		return convertDate(date);
    	}
    }
    
    /**
     * 计算2个日期的天数差
     * @param fDate
     * @param oDate
     * @return
     */
    public static int daysOfTwo(Date fDate, Date oDate) {

//        Calendar aCalendar = Calendar.getInstance();
//
//        aCalendar.setTime(fDate);
//
//        int day1 = aCalendar.get(Calendar.DAY_OF_YEAR);
//
//        aCalendar.setTime(oDate);
//
//        int day2 = aCalendar.get(Calendar.DAY_OF_YEAR);
//
//        return day2 - day1;

		Calendar calst = Calendar.getInstance();
		Calendar caled = Calendar.getInstance();
		calst.setTime(fDate);
		caled.setTime(oDate);
		//设置时间为0时
		calst.set(Calendar.HOUR_OF_DAY, 0);
		calst.set(Calendar.MINUTE, 0);
		calst.set(Calendar.SECOND, 0);
		caled.set(Calendar.HOUR_OF_DAY, 0);
		caled.set(Calendar.MINUTE, 0);
		caled.set(Calendar.SECOND, 0);
		//得到两个日期相差的天数
		int days = ((int) (caled.getTime().getTime() / 1000) - (int) (calst
				.getTime().getTime() / 1000)) / 3600 / 24;

		return days;

	}
    
    /**
     * 计算2个时间的小时差
     * @param lastDay
     * @param beforeDay
     * @return
     */
    public static long twoHoursOfDay(Date lastDay, Date beforeDay) {
    	
    	long nd = 1000 * 24 * 60 * 60;
	    long nh = 1000 * 60 * 60;
    	long diff = lastDay.getTime() - beforeDay.getTime();
    	
    	long hour = diff % nd / nh;
    	
    	return hour;
    }


    public static Boolean isStartHalfDay(Date startTimeStrTypeOne) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat(YMD8);
        Date d = sdf.parse("08:30:00");
        String startTimeStrTypeOneStr = convertToString(startTimeStrTypeOne, YMD8);
        String s1 = convertToString(d, YMD8);
        if(startTimeStrTypeOneStr.equals(s1)){
            return false;
        }else {
            return true;
        }
    }

    public static Boolean isEndHalfDay(Date startTimeStrTypeOne) throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat(YMD8);
        Date d = sdf.parse("17:00:00");
        String startTimeStrTypeOneStr = convertToString(startTimeStrTypeOne, YMD8);
        String s1 = convertToString(d, YMD8);
        if(startTimeStrTypeOneStr.equals(s1)){
            return false;
        }else {
            return true;
        }
    }

	public static String convertLocalToUtc(String localDateTime) {

		String utcTime = "";
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Date dateUtcTime = dateFormat.parse(localDateTime);

			utcTime = convertLocalToUtc(dateUtcTime);

		} catch (Exception e) {
			log.error("convertLocalToUtc error {}", e);
		}

		return utcTime;
	}

	public static String convertLocalToUtc(Date localDateTime) {

		String utcTime = "";

		if (localDateTime == null) return null;

		long longLocalTime = localDateTime.getTime();

		long longUtcTime = longLocalTime;

		//获取当前用户信息中保存的offset
		Integer offset = SubjectUtil.getCurrUser().getTzOffset();

		longLocalTime = longUtcTime + offset * 60 * 1000;

		SimpleDateFormat dateFormat = new SimpleDateFormat(YMD_FULL);

		utcTime = dateFormat.format(longLocalTime);

		return utcTime;
	}

	public static String convertUtcToLocal(String utcTime) {
		String localTime = "";

		if ( utcTime == null ) return null;

		SimpleDateFormat dateFormat = new SimpleDateFormat(YMD_FULL);

		try {
			// 표준시를 Date 포맷으로 변경
			Date dateUtcTime = dateFormat.parse(utcTime);

			localTime = convertUtcToLocal(dateUtcTime);

		} catch (Exception e) {
			log.error("convertUtcToLocal error {}", e);
		}

		return localTime;
	}

	public static String convertUtcToLocal(Date dateUtcTime) {
		String localTime = "";

		if ( dateUtcTime == null ) return null;

		SimpleDateFormat dateFormat = new SimpleDateFormat(YMD_FULL);

		long longUtcTime = dateUtcTime.getTime();

		Integer offset = SubjectUtil.getCurrUser().getTzOffset();

		long longLocalTime = longUtcTime - offset*60*1000;
		Date dateLocalTime = new Date();
		dateLocalTime.setTime(longLocalTime);

		localTime = dateFormat.format(dateLocalTime);

		return localTime;
	}

	public static void main(String[] args) throws Exception {

		for (int i=1;i <= 300 ;i ++) {
			System.out.print(i + " ");

		}
	}
}
