package com.youjitech.common.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class QRUtil {
 
 public static void makeQR(String url , int width , int height , String file_path , String file_name){
  try{
   File file = null;
   
   file = new File(file_path);
   if(!file.exists()){
    file.mkdirs();
   }
   QRCodeWriter writer = new QRCodeWriter();
   url = new String(url.getBytes("UTF-8") , "ISO-8859-1");
   BitMatrix matrix = writer.encode(url, BarcodeFormat.QR_CODE, width, height);
   //QR코드 색상
   int qrColor = 0xFFad1004;
   qrColor = 0x000000;
   MatrixToImageConfig config = new MatrixToImageConfig(qrColor , 0xFFFFFFFF);
   BufferedImage qrImage = MatrixToImageWriter.toBufferedImage(matrix , config);
    ImageIO.write(qrImage ,  "png" ,  new File(file_path+file_name));
  } catch (Exception e) {
   e.printStackTrace();
  }  
 }
 
 public static BufferedImage getQrcodeBufferImage(String content , int width , int height, int qrColor, int backColor){
	   BufferedImage qrImage = null;
	  try{
	   File file = null;
	  
	   QRCodeWriter writer = new QRCodeWriter();
	   content = new String(content.getBytes("UTF-8") , "ISO-8859-1");
	   BitMatrix matrix = writer.encode(content, BarcodeFormat.QR_CODE, width, height);
	   
	   MatrixToImageConfig config = new MatrixToImageConfig(qrColor ,backColor );
	   qrImage = MatrixToImageWriter.toBufferedImage(matrix , config);
	    
	  } catch (Exception e) {
	   e.printStackTrace();
	  }  
	  return qrImage;
	}
 
 
 public static ResponseEntity<byte[]> getQRCodeJPG(String content , int width , int height, int qrColor, int backColor) throws IOException{

	BufferedImage bf = QRUtil.getQrcodeBufferImage(content, width, height, qrColor, backColor);
	
	//byte[] imageBytes = ((DataBufferByte) bf.getData().getDataBuffer()).getData();
	
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	ImageIO.write( bf, "jpg", baos );
	baos.flush();
	byte[] imageInByte = baos.toByteArray();
	baos.close();

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.IMAGE_PNG);
	return new ResponseEntity<byte[]>(imageInByte, headers, HttpStatus.OK);
 }
 
 
}

