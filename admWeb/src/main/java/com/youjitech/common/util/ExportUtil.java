package com.youjitech.common.util;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

/**
 * 
 * 导出工具类
 * @version 1.0
 */
public class ExportUtil {
	
	public static void exportRamdonNo(final String ramdonNo,final HttpServletResponse response) throws IOException{
		exportRamdonNo(ramdonNo,20,26,3,null,response);
	}
	
	/**
	 * 
	 * @param ramdonNo 随机字符串
	 * @param width:字宽度
	 * @param height：字高度
	 * @param padding : 左右的间距像素
	 * @param font 字体
	 * @param response
	 * @throws IOException
	 */
	public static void exportRamdonNo(final String ramdonNo,final int width,final int height,final int padding,final Font font,final HttpServletResponse response) throws IOException{
		final int ramdonNoLen = ramdonNo.length();
		final int canvasWidth = width*ramdonNoLen+padding*2;
		final int canvasHeight = height;
		// 定义图像buffer   
		final BufferedImage image = new BufferedImage(canvasWidth, canvasHeight,
				BufferedImage.TYPE_INT_RGB);
		final Graphics2D gra = image.createGraphics();
		// 将图像填充为白色   
		gra.setColor(Color.WHITE);
		gra.fillRect(0, 0, canvasWidth, canvasHeight);

		// 创建字体，字体的大小应该根据图片的高度来定。  

		//字体对象构造方法public Font(String familyName,int style,int size)
		// familyName字体名；字体名可以分成两大类：中文字体：宋体、楷体、黑体等；英文字体：Arial、Times New Roman等等；
		// style风格。PLAIN普通字体，BOLD（加粗），ITALIC（斜体），Font.BOLD+ Font.ITALIC（粗斜体）
		//size 大小

		Font showFont = font;
		if(font==null){
			showFont = new Font("Fixedsys", Font.BOLD + Font.ITALIC, canvasHeight - 1);
		}

		// 设置字体。   
		gra.setFont(showFont);

		// 画边框。   
		gra.setColor(getRamdonColor());
		gra.drawRect(0, 0, canvasWidth - 1, canvasHeight - 1);

		// 随机产生干扰线，使图象中的认证码不易被其它程序探测到。   
		gra.setColor(Color.BLACK);
		final Random random = new Random();
		for (int i = 0; i < 30; i++) {
			int x = random.nextInt(canvasWidth);
			int y = random.nextInt(canvasHeight);
			int xl = random.nextInt(5);
			int yl = random.nextInt(5);
			gra.setColor(getRamdonColor());
			gra.drawLine(x, y, x + xl, y + yl);
		}

		for (int i = 0; i < ramdonNoLen; i++) {
			char c = ramdonNo.charAt(i);
			gra.drawString(c + "", i * width + padding, width); // 25为宽度，11为上下高度位置
		}
		
		setNoCacheHeaders(response);
		response.setContentType("image/gif");
		OutputStream os = null;
	    try {  
	    	os = response.getOutputStream();  
	    	response.reset();  

	    	ImageIO.write(image,"JPEG",os);
			
	        os.flush();  
	    } finally {  
	         if (os != null) {  
				os.close(); 
	        }  
	    } 

	}
	
	/**
	 * 
	 * 设定不保留cache
	 * 
	 * @param response
	 */
	public static void setNoCacheHeaders(final HttpServletResponse response) {
		// HTTP 1.1
		response.setHeader("Cache-Control",
				"no-store, no-cache, must-revalidate");

		// HTTP 1.0
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", -1);
	}
	
	/**
	 * 
	 * 获取随机颜色
	 * @return Color
	 */
	private static Color getRamdonColor() {
		Random random = new Random();
		int red = 0, green = 0, blue = 0;
		// 产生随机的颜色分量来构造颜色值，这样输出的每位数字的颜色值都将不同。   
		red = random.nextInt(155);
		green = random.nextInt(100);
		blue = random.nextInt(255);
		return new Color(red, green, blue);
	}
}
