package com.youjitech.common.util.sms;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jsms.api.SendSMSResult;
import cn.jsms.api.common.SMSClient;
import cn.jsms.api.common.model.SMSPayload;
import com.youjitech.common.property.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/7
 */
public class JGSMSUtil {


    private static Logger LOG = LoggerFactory.getLogger(JGSMSUtil.class);

    private static String APP_KEY_STUDENT = PropertiesLoader.getEnvValue("jg.app.key.student");

    private static String APP_SCRECT_STUDENT = PropertiesLoader.getEnvValue("jg.app.key.secret");

//    private static String APP_KEY_TEACHER = "955778acb2c9528faf630005";
//    private static String app_screct_teacher = "fa99d6e387f9d2aaad6502bb";

    private SMSClient smsClient;

    private JGSMSUtil() {}

    public static void sendJGSms(String mobile, int templateId, Map<String, String> params) {

        SMSClient client = initSmsClient();
        SMSPayload payload = SMSPayload.newBuilder()
                .setMobileNumber(mobile)
                .setTempId(templateId)
//                .addTempPara("phone", "021-67185230")
                .setTempPara(params)
                .build();
        try {
            SendSMSResult res = client.sendTemplateSMS(payload);
            LOG.info(res.toString());
        } catch (APIRequestException e) {
            LOG.error("Error response from JPush server. Should review and fix it. ", e);
            LOG.info("HTTP Status: " + e.getStatus());
            LOG.info("Error Message: " + e.getMessage());
        } catch (APIConnectionException e) {
            LOG.error("Connection error. Should retry later. ", e);
        }

    }

    private static SMSClient initSmsClient(){
        return new SMSClient(APP_SCRECT_STUDENT, APP_KEY_STUDENT);
    }


}

