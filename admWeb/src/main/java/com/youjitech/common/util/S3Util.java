package com.youjitech.common.util;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.youjitech.common.exception.YoujiException;
import com.youjitech.common.property.PropertiesLoader;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created By wangbo
 * 2019/2/21
 */
public class S3Util {

    private static String bucketName = PropertiesLoader.getEnvValue("s3.bucketName");
    private static String S3_VIEW_URL_PREFIX = PropertiesLoader.getEnvValue("s3.url") ;

    private static AmazonS3 s3;

    public final static String FOLDER_PREFIX_NOTICE = "notice";
    public final static String FOLDER_PREFIX_QNA = "qna";
    public final static String FOLDER_PREFIX_FAQ = "faq";
    public final static String FOLDER_PREFIX_USER = "user";
    public final static String FOLDER_PREFIX_SCORE_IMAGE = "score_image";

    public final static String PIC_SIZE_LARGE = "large";
    public final static String PIC_SIZE_THUMB = "thumb";
    public final static String PIC_SIZE_ORIGINAL = "original";
    public final static String PIC_SIZE_BANNER = "banner";

    public final static int PIC_SIZE_THUMB_WIDTH_HEIGHT = 100;

    public final static int PIC_SIZE_LARGE_WIDTH = 2208;
    public final static int PIC_SIZE_LARGE_HEIGHT = 1242;

    private static AmazonS3 getS3(){

        if(s3 == null){
            synchronized (S3Util.class) {
                String AWS_ACCESS_KEY = System.getProperty("AWS_ACCESS_KEY")==null?PropertiesLoader.getEnvValue("aws.access.key"):System.getProperty("AWS_ACCESS_KEY");
                String AWS_SECRET_KEY = System.getProperty("AWS_SECRET_KEY")==null?PropertiesLoader.getEnvValue("aws.secret.key"):System.getProperty("AWS_SECRET_KEY");

                s3 = new AmazonS3Client(new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY));
                //        s3.setEndpoint(endPoint);
                s3.setRegion(Region.getRegion(Regions.CN_NORTH_1));
            }
        }
        return s3;
    }

    public static HashMap<String, String> getS3ImageUrl(String host, String directory, int index, String fileName){

        return getS3ImageUrl(host, directory, index, fileName, false);
    }

    public static HashMap<String, String> getS3ImageUrl(String host, String directory, int index, String fileName, Boolean hasBanner){
        HashMap<String, String> rsMap = new HashMap<String, String>();
        rsMap.put("thumb", "");
        rsMap.put("large", "");
        rsMap.put("original", "");

        String partition = getS3IdPartition(index);

        if ( index > 0 && !StringUtils.isEmpty(fileName)) {
            rsMap.put("thumb", host + "/" + directory + "/" + partition + "thumb/" + fileName );
            rsMap.put("large", host + "/" + directory + "/" + partition + "large/" + fileName );
            rsMap.put("original", host + "/" + directory + "/" + partition + "original/" + fileName );
            if(hasBanner){
                rsMap.put("banner", host + "/" + directory + "/" + partition + "banner/" + fileName );
            }
        }

        return rsMap;
    }

    public static String getS3IdPartition(int index){
        String result = new String();
        result = "";

        String str = "000000000000" + String.valueOf(index);
        str = str.substring(str.length()-12);

        for(int i=0; i<4; i++) {
            result += str.substring(i*3, i*3+3) + "/";
        }

        return result;
    }

    public static void processImage(File uploadFile, int objectId, String folderPrefix) {

        String tmpPath = System.getProperty("java.io.tmpdir");

        String fileName = uploadFile.getName();

        String thumbFilePath = tmpPath + StringPool.SLASH + "t_" + fileName;

        String largeFilePath = tmpPath + StringPool.SLASH + "l_" + fileName;

        //original File
        try {

            //压缩图片 thumb
            ImageUtil.zoom(uploadFile.getPath(), thumbFilePath, S3Util.PIC_SIZE_THUMB_WIDTH_HEIGHT, S3Util.PIC_SIZE_THUMB_WIDTH_HEIGHT);

            //large
            ImageUtil.zoom(uploadFile.getPath(), largeFilePath, S3Util.PIC_SIZE_LARGE_WIDTH, S3Util.PIC_SIZE_LARGE_HEIGHT);

            //update
            //S3 original PATH
            String originalUploadPath = S3Util.generateFilePath(objectId, S3Util.PIC_SIZE_ORIGINAL, fileName);
            uploadToS3(uploadFile, originalUploadPath, folderPrefix);
            //thumb
            //S3 thumb PATH
            //S3 thumb PATH
            String thumbUploadPath = S3Util.generateFilePath(objectId, S3Util.PIC_SIZE_THUMB, fileName);
            //thumb temp file
            File thumbFile = new File(thumbFilePath);
            uploadToS3(thumbFile, thumbUploadPath, folderPrefix);
            //large
            //S3 large PATH
            String largeUploadPath = S3Util.generateFilePath(objectId, S3Util.PIC_SIZE_LARGE, fileName);
            //thumb temp file
            File largeFile = new File(largeFilePath);
            uploadToS3(largeFile, largeUploadPath, folderPrefix);
        }catch(Exception e) {
            e.printStackTrace();
            throw new YoujiException(e.getMessage());
        }
    }

    //生成最终文件路径
    public static String generateFilePath(Integer id, String sizePrefix, String fileName) {

        String pathPrefix = getS3IdPartition(id);

        String fileFullPath = StringPool.SLASH + pathPrefix + sizePrefix + StringPool.SLASH +  fileName;

        return fileFullPath;
    }

    public static String uploadToS3(File tempFile, String remoteFileName, String prefixPath) throws IOException {
        try {

            remoteFileName = prefixPath + remoteFileName;

            //上传文件
            getS3().putObject(new PutObjectRequest(bucketName, remoteFileName, tempFile).withCannedAcl(CannedAccessControlList.PublicRead));
            //获取一个request
            GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(
                    bucketName, remoteFileName);
            //生成公用的url
            URL url = getS3().generatePresignedUrl(urlRequest);
            System.out.println("=========URL=================" + url + "============URL=============");
            return url.toString();
        } catch (AmazonServiceException ase) {
            ase.printStackTrace();
        } catch (AmazonClientException ace) {
            ace.printStackTrace();
        }
        return null;
    }


    public static void main(String[] args) throws Exception {

        uploadToS3(new File("F:\\pic\\1_tmp.jpg"), "/000/000/000/040/banner/2.jpg", "notice");
    }
}
