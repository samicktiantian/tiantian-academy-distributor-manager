package com.youjitech.system.code.entity;

import com.youjitech.system.base.entity.NoIdxEntity;
import lombok.Data;

/**
 * Created By wangbo
 * 2019/1/22
 */
@Data
public class Code extends NoIdxEntity {

    private Integer cdDist;
    private Integer cdCode;
    private String cdNameKo;
    private String cdNameZhCn;
    private String cdNameEn;
    private String cdLevel;
    private String cdOption1;

    public String getCdName(String language) {
        switch(language.toLowerCase()) {
            case "ko_kr":
            case "ko":
            case "kr":
                language = "ko";
                return cdNameKo;
            case "en":
                language = "en";
                return cdNameEn;
            default:
                return cdNameZhCn;
        }
    }
}
