package com.youjitech.system.code.service;

import com.youjitech.common.Constant;
import com.youjitech.system.base.service.BaseService;
import com.youjitech.system.code.entity.Code;
import com.youjitech.system.code.mapper.CodeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/22
 */
@Service
public class CodeService extends BaseService<Code, Integer> {
    
    @Autowired
    private CodeMapper codeMapper;

    private volatile  Map<Integer,List<Code>> codeMap;
    
    public Map<Integer, List<Code>> getAllCodeMap() {
        buildMap();
        return codeMap;
    }

    private void buildMap(){
        if(codeMap==null){
            synchronized(CodeService.class){
                if(codeMap==null){
                    codeMap = new LinkedHashMap<Integer,List<Code>>();
                    Code condition = new Code();
                    condition.setUse(Constant.FLAG_STRING_Y);
                    List<Code> list = codeMapper.findList(condition);
                    for(Code code : list) {
                        List<Code> pkList = (List<Code>) codeMap.get(code.getCdDist());
                        if(pkList==null){
                            pkList = new ArrayList<Code>();
                            codeMap.put(code.getCdDist(),pkList);
                        }
                        pkList.add(code);
                    }
                }
            }
        }
    }

    public List<Code> findByCdDict(Integer cdDict) {
        Code condition = new Code();
        condition.setCdDist(cdDict);
        condition.setUse(Constant.FLAG_STRING_YES);
        List<Code> codeList = codeMapper.findList(condition);
        return codeList;
    }

    public void reloadCache() {
        codeMap = null;
        buildMap();
    }

    @Override
    public void add(Code code) {
        Code condition = new Code();
        condition.setCdCode(code.getCdCode());
        condition.setCdDist(code.getCdDist());

        Code originalData = codeMapper.getByCondition(condition);
        originalData.setCdNameZhCn(code.getCdNameZhCn());
        originalData.setCdNameEn(code.getCdNameEn());
        originalData.setCdNameKo(code.getCdNameKo());
        originalData.setCdLevel(code.getCdLevel());
        originalData.setUse(code.getUse());

        codeMapper.update(originalData);

        reloadCache();
    }
}
