package com.youjitech.system.code.util;

import com.youjitech.system.code.entity.Code;

import java.util.List;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/22
 */
public class CodeUtil {

    public static Map<String, String> getValue(final Integer cdDist, final Integer code) {
        if ( cdDist==null || code==null ) return null;
        Map<Integer, Map<String, String>> map = CodeLoader.getInstance().getMap(cdDist);
        return map.get(code);
    }

    public static void reload() {
        CodeLoader.getInstance().reload();
    }

    public static Code getDict(Integer cdDist, Integer code){
        return CodeLoader.getInstance().getCode(cdDist, code);
    }

    public static List<Code> getDictList(Integer cdDict) {
        return CodeLoader.getInstance().getCodeList(cdDict);
    }

    public static Code getCode(Integer cdDist, Integer code) {
        Code codeVO = getDict(cdDist, code);
        return codeVO;
    }
}
