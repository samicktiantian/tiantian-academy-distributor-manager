package com.youjitech.system.code.controller;

import com.youjitech.system.base.controller.BaseFunctionController;
import com.youjitech.system.code.entity.Code;
import com.youjitech.system.code.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created By wangbo
 * 2019/1/22
 */
@Controller
@RequestMapping("code")
public class CodeController extends BaseFunctionController<Code, Integer> {

    @Autowired
    private CodeService codeService;

    @PostMapping("add")
    public ResponseEntity<?> add(Code code){
        codeService.add(code);
        return new ResponseEntity<>(getSuccessCode(), HttpStatus.OK);
    }

    @GetMapping("view/{cdDist}/{cdCode}")
    public ModelAndView view(@PathVariable Integer cdDist, @PathVariable Integer cdCode){
        Code condition = new Code();
        condition.setCdDist(cdDist);
        condition.setCdCode(cdCode);
        Code code = codeService.getByCondition(condition);
        return generateModelAndView(VIEW_OBJECT_KEY, code, toPage(VIEW_PAGE_ADD));
    }

}
