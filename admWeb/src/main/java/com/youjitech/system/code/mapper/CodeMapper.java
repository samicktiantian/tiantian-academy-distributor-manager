package com.youjitech.system.code.mapper;

import com.youjitech.system.base.mapper.BaseMapper;
import com.youjitech.system.code.entity.Code;

/**
 * Created By wangbo
 * 2019/1/22
 */
public interface CodeMapper extends BaseMapper<Code, Integer> {
}
