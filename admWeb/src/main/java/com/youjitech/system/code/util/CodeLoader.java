package com.youjitech.system.code.util;

import com.google.common.collect.Maps;
import com.youjitech.common.Constant;
import com.youjitech.common.util.SpringUtils;
import com.youjitech.system.code.entity.Code;
import com.youjitech.system.code.service.CodeService;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/22
 */
@Slf4j
public class CodeLoader {

    protected Map<Integer, Map<Integer, Map<String, String>>> cdDictMap = new HashMap<Integer, Map<Integer, Map<String, String>>>();
    protected Map<Integer, Map<Integer, Code>> cdDictEntityMap = new HashMap<Integer, Map<Integer, Code>>();
    protected Map<Integer, List<Code>> codeMap = new HashMap<Integer, List<Code>>();

    public static CodeLoader instance;

    public Map<Integer, Map<Integer, Map<String, String>>> getCdDictMap() {
        return cdDictMap;
    }

    public Map<Integer, Map<Integer, Code>> getCdDictEntityMap() {
        return cdDictEntityMap;
    }

    private void initCodeMap(){
        final CodeService codeService = SpringUtils.getBean(CodeService.class);
        codeMap = codeService.getAllCodeMap();
    }

    private CodeLoader() {

        initCodeMap();

        for (final Integer cdDict : codeMap.keySet()) {
            List<Code> codeList = codeMap.get(cdDict);

            Map<Integer, Map<String, String>> map = new LinkedHashMap<Integer, Map<String, String>>();
            Map<Integer, Code> vomap = new LinkedHashMap<Integer, Code>();

            cdDictMap.put(cdDict, map);
            cdDictEntityMap.put(cdDict, vomap);

            for (final Code code : codeList) {
                Map<String, String> cdNameMap = Maps.newHashMap();
                cdNameMap.put(Constant.LANG_KO, code.getCdNameKo());
                cdNameMap.put(Constant.LANG_ZH_CN, code.getCdNameZhCn());
                cdNameMap.put(Constant.LANG_EN, code.getCdNameEn());

                map.put(code.getCdCode(), cdNameMap);
                vomap.put(code.getCdCode(), code);
            }
        }
    }

    public static CodeLoader getInstance() {
        if (instance == null) {
            synchronized (CodeLoader.class) {
                if (instance == null) {
                    instance = new CodeLoader();
                }
            }
        }
        return instance;
    }

    public void reload() {
        final CodeService codeService = (CodeService) SpringUtils.getBean(CodeService.class);
        log.warn("start load code cache ........");
        codeService.reloadCache();
        instance = null;
        getInstance();
        log.warn("finish load code cache");
    }

    public void reload(final Integer cdDict) {
        final CodeService codeService = (CodeService) SpringUtils.getBean(CodeService.class);
        log.warn("start load code " + cdDict + " cache ........");
        final List<Code> codeList = codeService.findByCdDict(cdDict);

        final Map<Integer, Map<String, String>> tempmap = new LinkedHashMap<>();
        final Map<Integer, Code> tempvomap = new LinkedHashMap<>();
        for (final Code code : codeList) {
            Map<String, String> cdNameMap = Maps.newHashMap();
            cdNameMap.put(Constant.LANG_KO, code.getCdNameKo());
            cdNameMap.put(Constant.LANG_ZH_CN, code.getCdNameZhCn());
            cdNameMap.put(Constant.LANG_EN, code.getCdNameEn());

            tempmap.put(code.getCdCode(), cdNameMap);
            tempvomap.put(code.getCdCode(), code);
        }

        final Map<Integer, Map<String, String>> strMap = CodeLoader.getInstance().getMap(cdDict);
        final Map<Integer, Code> voMap = CodeLoader.getInstance().getEntityMap(cdDict);

        if (strMap == null) {
            CodeLoader.getInstance().getCdDictMap().put(cdDict, tempmap);
        } else {
            strMap.clear();
            strMap.putAll(tempmap);
        }

        if (voMap == null) {
            CodeLoader.getInstance().getCdDictEntityMap().put(cdDict, tempvomap);
        } else {
            voMap.clear();
            voMap.putAll(tempvomap);
        }

        log.warn("finish load dict " + cdDict + " cache");
    }

    public Map<Integer, Map<String, String>> getMap(final Integer cdDict) {
        return cdDictMap.get(cdDict);
    }

    public Map<Integer, Code> getEntityMap(final Integer cdDict) {
        return cdDictEntityMap.get(cdDict);
    }

    public Code getCode(final Integer cdDict, final Integer code) {
        Map<Integer, Code> entityMap = cdDictEntityMap.get(cdDict);
        Code d = entityMap.get(code);
        return d;
    }

    public Map<Integer, String> geValidtMap(final Integer cdDict) {
        final Map<Integer, Code> vomap = getValidMapEntity(cdDict);
        final Map<Integer, String> map = new LinkedHashMap<>();
        for (final Integer code : vomap.keySet()) {
            final Code entity = vomap.get(code);
            map.put(code, entity.getCdNameZhCn());
        }
        return map;
    }

    public Map<Integer, Code> getValidMapEntity(final Integer cdDict) {
        final Map<Integer, Code> vomap = getEntityMap(cdDict);
        final Map<Integer, Code> map = new LinkedHashMap<>();
        for (final Integer code : vomap.keySet()) {
            final Code entity = vomap.get(code);
            if (entity.getUse().equals(Constant.FLAG_STRING_YES)) {
                map.put(code, entity);
            }
        }
        return map;
    }

    public List<Code> getCodeList (final Integer cdDict){
        return codeMap.get(cdDict);
    }
}
