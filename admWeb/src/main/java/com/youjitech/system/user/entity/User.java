package com.youjitech.system.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.youjitech.system.base.entity.BaseEntity;
import lombok.Data;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Data
public class User extends BaseEntity {

    private String loginName;
    private String realName;
    private String email;
    private String mobile;
    @JsonIgnoreProperties
    private String password;
    @JsonIgnoreProperties
    private String salt;
    private String status;
    private String language;

    private Integer roleIdx;

}
