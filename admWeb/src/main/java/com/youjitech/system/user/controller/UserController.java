package com.youjitech.system.user.controller;

import com.google.common.collect.Maps;
import com.youjitech.system.base.controller.BaseFunctionController;
import com.youjitech.system.role.service.RoleService;
import com.youjitech.system.user.entity.User;
import com.youjitech.system.user.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/17
 */
@Controller
@RequestMapping(value = "user")
public class UserController extends BaseFunctionController<User, Integer> {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    private Map<String, Object> selList(){
        Map<String, Object> resultMap = Maps.newHashMap();
        resultMap.put("roleList", roleService.findAll());
        return resultMap;
    }

    @GetMapping("view/{userId}")
    public ModelAndView view(@PathVariable Integer userId){
        User user = userService.findByIdx(userId);
        Map<String, Object> resultMap = selList();
        resultMap.put("user", user);
        return generateModelAndView(resultMap, toPage(VIEW_PAGE_ADD));
    }

    @Override
    protected Map<String, Object> getOtherDataMapForAdd() {
        return selList();
    }

    @RequiresPermissions(value = "user:add")
    @PostMapping(value = "/add")
    public ResponseEntity<?> userAdd(User user){

        userService.addUser(user);
        return new ResponseEntity<>(getSuccessCode(), HttpStatus.OK);
    }

    @RequiresPermissions(value = "user:delete")
    @GetMapping(value = "/del/{idx}")
    public ResponseEntity<?> userDel(@PathVariable Integer idx){

        userService.delete(idx);
        return new ResponseEntity<>(getSuccessCode(), HttpStatus.OK);
    }

    @GetMapping(value = "toChangePwdpage")
    public ModelAndView toChangePwdpage(){
        return generateModelAndView(toPage("changePwd"));
    }

    @PostMapping(value="modifyPassword")
    public ResponseEntity<?> modifyPassword(@RequestParam Map<String, String> paramMap) {
        userService.modifyPassword(getCurrentUser().getIdx(), paramMap);
        return generateSuccessResponseEntity();
    }

    @GetMapping(value = "viewOwn")
    public ModelAndView viewOwn(){
        User user = userService.findByIdx(getCurrentUserId());
        Map<String, Object> resultMap = selList();
        resultMap.put("user", user);
        return generateModelAndView(resultMap, toPage("viewOwn"));
    }

    @PostMapping(value = "updateOwn")
    public ResponseEntity<?> updateOwn(User user){
        userService.updateOwn(user);
        return new ResponseEntity<>(getSuccessCode(), HttpStatus.OK);
    }
}
