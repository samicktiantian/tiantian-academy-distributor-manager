package com.youjitech.system.user.service;

import com.youjitech.common.exception.ExceptionUtil;
import com.youjitech.system.base.service.BaseService;
import com.youjitech.system.role.entity.UserRole;
import com.youjitech.system.role.mapper.RoleMapper;
import com.youjitech.system.role.mapper.UserRoleMapper;
import com.youjitech.system.user.entity.User;
import com.youjitech.system.user.mapper.UserMapper;
import org.apache.commons.lang3.StringUtils;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Service
public class UserService extends BaseService<User, Integer> {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private RoleMapper roleMapper;


    public User findByLoginName(String username) {
        return userMapper.findByLoginName(username);
    }

    public boolean validateUserResourcePermission(User user) {

        List<User> list = userMapper.validateUserResourcePermission(user.getIdx());

        return list.size() > 0;
    }

    public User findByIdx(Integer idx) {
        User user = userMapper.findByIdx(idx);
        List<Integer> roleIdxByUserIdx = userRoleMapper.findRoleIdxByUserIdx(user.getIdx());
        if(roleIdxByUserIdx != null && roleIdxByUserIdx.size() > 0){
            user.setRoleIdx(roleIdxByUserIdx.get(0));
        }
        return user;
    }

    @Transactional
    public void addUser(User user) {

        boolean addRoleFlag = true;
        Integer userIdx = null;

        if(user.getIdx() != null){
            //update
            User originalData = setCommonProperty(user);

            //validate loginname
            if(!user.getLoginName().equals(originalData.getLoginName())) {
                validateLoginName(user.getLoginName());
                originalData.setLoginName(user.getLoginName());
            }

//            //update password
//            if(!StringUtils.isEmpty(user.getPassword())){
//                originalData.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
//            }

            originalData.setRoleIdx(user.getRoleIdx());
            originalData.setUse(user.getUse());
            originalData.setStatus(user.getStatus());
            originalData.setAmCode(user.getAmCode());
            userMapper.update(originalData);
            //update user role
            List<Integer> userRoleIdxList = userRoleMapper.findRoleIdxByUserIdx(user.getIdx());
            if(userRoleIdxList != null && userRoleIdxList.size() > 0){
                //暂时用户角色只有一个,所以取第一条记录做对比即可
                Integer originalRoleIdx = userRoleIdxList.get(0);
                if(originalRoleIdx.intValue() == user.getRoleIdx().intValue()){
                    addRoleFlag = false;
                }else{
                    userRoleMapper.deleteByUserIdx(user.getIdx());
                }
            }
            userIdx = originalData.getIdx();
        }
        else {
            //add
            validateLoginName(user.getLoginName());

            user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));

            userMapper.add(user);

            userIdx = user.getIdx();
        }

        if(addRoleFlag) {
            UserRole ur = new UserRole();
            ur.setUserIdx(userIdx);
            ur.setRoleIdx(user.getRoleIdx());
            userRoleMapper.add(ur);
        }

    }

    private void validateLoginName(String loginName){
        User userByLoginName = userMapper.findByLoginName(loginName);
        if(userByLoginName != null){
            ExceptionUtil.checkException("exception.user.exists");
        }
    }

    private void entryptPassword(User user) {
//        byte[] salt = Digests.generateSalt(ShiroRDbRealm.SALT_SIZE);
//        user.setSalt(Encodes.encodeHex(salt));
//
//        byte[] hashPassword = Digests.sha1(user.getPassword().getBytes(), salt, ShiroRDbRealm.HASH_INTERATIONS);
//        user.setPassword(Encodes.encodeHex(hashPassword));


    }

    @Override
    @Transactional
    public void delete(Integer idx) {
        //del user
        userMapper.delete(idx);
        // del user role
        userRoleMapper.deleteByUserIdx(idx);
    }

    public void modifyPassword(Integer idx, Map<String, String> paramMap) {
        User user = userMapper.findByIdx(idx);
        String oldPasswd = paramMap.get("oldPassword");

         if(!BCrypt.checkpw(oldPasswd, user.getPassword())) {
             ExceptionUtil.checkException("exception.oldpassword.wrong");
         }

//        byte[] oldPasswordByte = Digests.sha1(oldPasswd.getBytes(), Encodes.decodeHex(user.getSalt()), ShiroRDbRealm.HASH_INTERATIONS);
//        String oldPassword = Encodes.encodeHex(oldPasswordByte);
//        if(!oldPassword.equals(user.getPassword())){
//            throw new YoujiException("exception.oldpassword.wrong");
//        }

        String newPasswd = paramMap.get("password");

        user.setPassword(BCrypt.hashpw(newPasswd, BCrypt.gensalt()));

        userMapper.update(user);
    }

    private User setCommonProperty(User newData){

        User originalData = userMapper.findByIdx(newData.getIdx());

        if(!StringUtils.isEmpty(newData.getPassword())) {
            originalData.setPassword(BCrypt.hashpw(newData.getPassword(), BCrypt.gensalt()));
        }
        originalData.setEmail(newData.getEmail());
        originalData.setMobile(newData.getMobile());
        originalData.setRealName(newData.getRealName());
        originalData.setLanguage(newData.getLanguage());

        return originalData;
    }

    public void updateOwn(User user) {

        User originalData = setCommonProperty(user);

        userMapper.update(originalData);

    }

}
