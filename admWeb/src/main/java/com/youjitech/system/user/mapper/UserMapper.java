package com.youjitech.system.user.mapper;

import com.youjitech.system.base.mapper.BaseMapper;
import com.youjitech.system.user.entity.User;

import java.util.List;

/**
 * Created By wangbo
 * 2019/1/15
 */
public interface UserMapper extends BaseMapper<User, Integer> {

    User findByLoginName(String username);

    List<User> validateUserResourcePermission(Integer idx);
}
