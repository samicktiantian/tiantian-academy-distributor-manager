package com.youjitech.system.base.vo;

import com.youjitech.system.base.entity.NoIdxEntity;
import lombok.Data;

import java.util.List;

/**
 * Created By wangbo
 * 2019/1/17
 */
@Data
public class DataGrid<M extends NoIdxEntity> {
    private List<M> rows;
    private long total;

    public DataGrid(List<M> rows) {
        this.rows = rows;
    }

    public DataGrid(List<M> rows, long total) {
        this.rows = rows;
        this.total = total;
    }

    public DataGrid() {
    }
}
