package com.youjitech.system.base.mapper;

import com.github.pagehelper.Page;
import com.youjitech.system.base.entity.NoIdxEntity;

import java.util.List;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/15
 */
public interface BaseMapper<M extends NoIdxEntity, ID> {

    /**
     * 新增方法
     */
    public void add(M m);

    /**
     * 删除
     */
    public void delete(ID id);

    /**
     * 更新对象
     */
    public void update(M m);

    /**
     * 查找单个对象
     */
    public M findByIdx(ID id);

    /**
     * 根据条件查询
     */
    public Page<M> findByCondition(Map<String, Object> params);

    /**
     * 根据条件删除数据
     */
    public void deleteByCondition(Map<String, Object> params);

    /**
     * find all data
     */
    public List<M> findAll();

    /**
     * find list by M condition
     *
     * @param m
     * @return
     */
    public List<M> findList(M m);

    /**
     * find list by Map parameters
     *
     * @param params
     * @return
     */
    public List<M> findList(Map<String, Object> params);

    /**
     * find single data object by condition
     *
     * @param m
     * @return
     */
    public M getByCondition(M m);


}
