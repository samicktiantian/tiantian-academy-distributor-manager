package com.youjitech.system.base.entity;

import com.youjitech.common.util.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created By wangbo
 * 2019/1/22
 */
@Data
public class NoIdxEntity implements Serializable {

    protected Date dtRegist;

    protected Date dtUpdate;

    protected String use;

    protected Integer aamIdx;

    protected Integer amIdx;

    protected Integer amCode;

    public String getDtRegistLocalTime(){
        return DateUtil.convertUtcToLocal(dtRegist);
    }

    public String getDtUpdateLocalTime() {return DateUtil.convertUtcToLocal(dtUpdate);}
}
