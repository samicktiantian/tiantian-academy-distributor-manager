package com.youjitech.system.base.controller;

import com.google.common.collect.Maps;
import com.youjitech.common.Constant;
import com.youjitech.common.util.StringPool;
import com.youjitech.system.role.realms.ShiroRDbRealm.ShiroUser;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/16
 */
public class BaseController {

    protected Map<String, Object> getSuccessCode(){
        Map<String, Object> map = Maps.newHashMap();
        map.put("code", Constant.SUCCESS_CODE);
        return map;
    }

    protected Map<String, Object> getSuccessMap(){
        Map<String, Object> map = Maps.newHashMap();
        map.put("code", Constant.SUCCESS_CODE);
        return map;
    }

    protected ResponseEntity<?> generateSuccessResponseEntity(String key, List list) {
        Map<String, Object> successMap = getSuccessMap();
        successMap.put(key, list);
        return new ResponseEntity<>(successMap, HttpStatus.OK);
    }

    protected ResponseEntity<?> generateSuccessResponseEntity(String key, Object obj) {
        Map<String, Object> successMap = getSuccessMap();
        successMap.put(key, obj);
        return new ResponseEntity<>(successMap, HttpStatus.OK);
    }

    protected ResponseEntity<?> generateSuccessResponseEntity() {
        return new ResponseEntity<>(getSuccessMap(), HttpStatus.OK);
    }


    protected Map<String, Object> getFailCode(){
        Map<String, Object> map = Maps.newHashMap();
        map.put("code", Constant.FAIL_CODE);
        return map;
    }

    protected Map<String, Object> getUnLogin(){
        Map<String, Object> map = Maps.newHashMap();
        map.put("code", Constant.UNLOGIN_CODE);
        return map;
    }

    protected ModelAndView generateModelAndView(String key, Object value, String viewName) {
        ModelAndView modelAndView = new ModelAndView();
        if (!StringUtils.isEmpty(key) && value != null) {
            modelAndView.addObject(key, value);
        }
        modelAndView.setViewName(viewName);
        return modelAndView;
    }

    protected ModelAndView generateModelAndView(Map<String, Object> values, String viewName) {
        ModelAndView modelAndView = new ModelAndView();
        if (values != null) {
            modelAndView.addAllObjects(values);
        }
        modelAndView.setViewName(viewName);
        return modelAndView;
    }

    protected ModelAndView generateModelAndView(String viewName) {
        return generateModelAndView(null, null, viewName);
    }

    protected Integer getCurrentUserId() {
        ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
        if (user == null)
            return null;
        return user.getIdx();
    }

    protected ShiroUser getCurrentUser() {
        ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
        if (user == null)
            return null;
        return user;
    }

    protected boolean hasRole(String role){
        return SecurityUtils.getSubject().hasRole(role);
    }

    protected boolean hasRole(List<String> roles){
        boolean[] result = SecurityUtils.getSubject().hasRoles(roles);
        for(boolean role : result){
            if(role){
                return true;
            }
        }
        return false;
    }

    protected boolean hasPermission(List<String> permissions){
        for(String permissionStr : permissions) {
            if(SecurityUtils.getSubject().isPermitted(permissionStr)){
                return true;
            }
        }
        return false;
    }

    protected boolean hasPermission(String permission){
        return SecurityUtils.getSubject().isPermitted(permission);
    }

    protected void checkPermission(List<String> permissions){
        for(String permissionStr : permissions) {
            SecurityUtils.getSubject().checkPermission(permissionStr);
        }
    }

    protected void checkPermission(String permission){
        SecurityUtils.getSubject().checkPermission(permission);
    }

    protected String toPage(String suffix){
        String simpleClassName = getClass().getName();
        //截取最后一段
        simpleClassName = simpleClassName.substring(simpleClassName.lastIndexOf(StringPool.PERIOD) + 1);
        //去掉controller
        simpleClassName = simpleClassName.replace("Controller", "");
        simpleClassName = simpleClassName.substring(0, 1).toLowerCase() + simpleClassName.substring(1);
        if(!StringUtils.isEmpty(suffix)){
            return simpleClassName + "/" + suffix;
        }else{
            return simpleClassName;
        }
    }
}
