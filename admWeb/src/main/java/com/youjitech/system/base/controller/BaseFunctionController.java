package com.youjitech.system.base.controller;

import com.github.pagehelper.Page;
import com.google.common.collect.Maps;
import com.youjitech.common.Constant;
import com.youjitech.common.exception.ExceptionUtil;
import com.youjitech.common.util.StringPool;
import com.youjitech.common.util.UploadUtil;
import com.youjitech.system.base.entity.NoIdxEntity;
import com.youjitech.system.base.service.BaseService;
import com.youjitech.system.base.vo.DataGrid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.Serializable;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/17
 */
@Slf4j
public abstract class BaseFunctionController<M extends NoIdxEntity, ID extends Serializable> extends BaseController {

    protected static String VIEW_OBJECT_KEY = "data";

    protected static String VIEW_OBJECT_KEY_AMCODE = "amCode";

    protected static String VIEW_PAGE_ADD = "add";

    protected static String VIEW_PAGE_VIEW = "view";

    protected static String VIEW_PAGE_LIST = "list";

    protected static String VIEW_PAGE_MODEL = "viewModel";

    @Autowired
    private BaseService<M, ID> baseService;

    @RequestMapping("init")
    @ResponseBody
    public ResponseEntity<?> init(HttpServletRequest request) {

        if(needAuth()) {
            //判断权限
            String prefix = toPage("");
            if (!hasPermission(prefix + StringPool.COLON + VIEW_PAGE_VIEW)) {
                ExceptionUtil.checkException("exception.nopermission");
            }
        }

        Page<M> pageList = baseService.findByCondition(initParams(request));

        return generateDataGrid(pageList);
    }

    /**
     * 用于返回datagrid数据
     * @param pageList
     * @return
     */
    protected ResponseEntity<?> generateDataGrid(Page pageList) {

        DataGrid dg = new DataGrid();
        dg.setRows(pageList.getResult());
        dg.setTotal(pageList.getTotal());

        return new ResponseEntity<DataGrid>(dg, HttpStatus.OK);
    }

    protected boolean needAuth(){
        return true;
    }

    @RequestMapping(value="list")
    public String list() {
        return toPage(VIEW_PAGE_LIST);
    }

    @RequestMapping(value = "listForWindow")
    public ModelAndView listForWindow() {
        return  generateModelAndView(VIEW_PAGE_MODEL, Constant.FLAG_STRING_Y, toPage(VIEW_PAGE_LIST));
    }


    protected Map<String, Object> initParams(HttpServletRequest request) {

        Map<String, Object> params = Maps.newHashMap();

        Map<String, String[]> parameterMap = request.getParameterMap();
        for (String dataKey : parameterMap.keySet()) {

            String value = parameterMap.get(dataKey)[0];
            //分页 start
            /*if("page".equals(dataKey)){
                params.put("page", value);
            } else*/
            if ("rows".equals(dataKey)) {
                params.put("pageSize", value);
            } /*else if("sort".equals(dataKey)){
                //分页 end
                //排序 start
                pageInfo.setSortColumn(value);
            } else if("order".equals(dataKey)){
                if("desc".equals(value)){
                    pageInfo.setSortType(Direction.DESC);
                }
            }*/ else {
                /**
                 * key和entity字段对应
                 */
                params.put(dataKey, value);
            }
            //参数end
            // 加入am_code过滤条件
//            params.put("amCode", getCurrentUser().getAmCode());

        }

        return params;
    }

    @RequestMapping(value = "viewForWindow/{idx}")
    public ModelAndView viewForWindow(@PathVariable ID idx) {
        Map<String, Object> viewFlagData = Maps.newHashMap();
        viewFlagData.put(VIEW_PAGE_MODEL, Constant.FLAG_STRING_Y);

        return generateForModelAndView(idx, viewFlagData);
    }

    @GetMapping(value = "viewAndValidate/{idx}")
    public ModelAndView viewAndValidate(@PathVariable ID idx){
       return generateForModelAndView(idx, null);
    }

    private ModelAndView generateForModelAndView(ID idx, Map<String, Object> otherData) {

        if(needAuth()) {
            //判断权限
            String prefix = toPage("");
            if(!hasPermission(prefix + StringPool.COLON + VIEW_PAGE_VIEW)){
                ExceptionUtil.checkException("exception.nopermission");
            }
        }

        M data = validateDataExists(idx);
        //验证是否有权限
        validateAmCode(data.getAmCode());

        Map<String, Object> otherDataMap = getOtherDataMapForView(data);
        otherDataMap.put(VIEW_OBJECT_KEY, data);
//        otherDataMap.put(VIEW_OBJECT_KEY_AMCODE, getCurrentUser().getAmCode());

        if(otherData != null) {
            otherDataMap.putAll(otherData);
        }

        return generateModelAndView(otherDataMap, toPage(VIEW_PAGE_ADD));
    }

    @GetMapping(value = "deleteAfterValidate/{idx}")
    public ResponseEntity<?> deleteAfterValidate(@PathVariable ID idx) {
        M data = validateDataExists(idx);
        validateAmCode(data.getAmCode());

        baseService.delete(idx);
        return generateSuccessResponseEntity();
    }

    @GetMapping(value = "toAdd")
    public ModelAndView toAdd(){
        //权限判断
        //获取 根权限值

        if(needAuth()) {
            String prefix = toPage("");
            if(!hasPermission(prefix + StringPool.COLON + VIEW_PAGE_ADD)){
                ExceptionUtil.checkException("exception.nopermission");
            }
        }

        Map<String, Object> otherDataMapForAdd = getOtherDataMapForAdd();
        //保存amcode
//        otherDataMapForAdd.put(VIEW_OBJECT_KEY_AMCODE, getCurrentUser().getAmCode());

        return generateModelAndView(otherDataMapForAdd, toPage(VIEW_PAGE_ADD));
    }

    protected Map<String, Object> getOtherDataMapForAdd(){
        //子类重写传递参数方法
        return Maps.newHashMap();
    }

    protected Map<String, Object> getOtherDataMapForView(M m){
        //子类重写传递参数方法
        return Maps.newHashMap();
    }

    protected M validateDataExists(ID idx){
        M data = baseService.findByIdx(idx);
        if(data == null) {
            ExceptionUtil.checkException("exception.data.notexists.or.nopermission");
        }
        return data;
    }

    protected void validateAmCode(Integer amCode){
//        if(amCode != null && getCurrentUser().getAmCode() !=null && amCode.intValue() != getCurrentUser().getAmCode().intValue()){
//            ExceptionUtil.checkException("exception.data.notexists.or.nopermission");
//        }
    }

    @GetMapping(value = "toLoading")
    public ModelAndView toLoadingPage(){
        return generateModelAndView("loading");
    }

    @GetMapping(value = "toDetailPage/{uIdx}")
    public ModelAndView toDetailPage(@PathVariable Integer uIdx){
        String prefix = toPage("");
        String returnPage = "detail." + prefix;
        return generateModelAndView("uIdx", uIdx, toPage(returnPage));
    }


    @RequestMapping(value="uploadFile")
    public ResponseEntity<?> uploadFile(HttpServletRequest request) {

        Map<String, Object> result = getSuccessMap();

        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();

        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            try{

                MultipartFile uploadFile = entity.getValue();

                // 获取保存文件夹路径
                String tmpPath = System.getProperty("java.io.tmpdir");

                String[] path = UploadUtil.getCreatePathWithSuffix(tmpPath, uploadFile.getOriginalFilename(), "myttpl");

                File f = new File(path[0]);

                uploadFile.transferTo(f);

                result.put("filePath", path[0]);

                result.put("fileName", uploadFile.getOriginalFilename());

                processUploadFile(f, request);

            }catch(Exception e){
                log.error("上传文件错误", e);
                result.put("code", Constant.FAIL_CODE);
                continue;
            }
        }

        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    }

    protected void processUploadFile(File f, HttpServletRequest request){
        //TODO
    }


}
