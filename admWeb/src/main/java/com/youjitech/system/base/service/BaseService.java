package com.youjitech.system.base.service;

import com.github.pagehelper.Page;
import com.google.common.collect.Maps;
import com.youjitech.common.util.PageUtil;
import com.youjitech.system.base.entity.NoIdxEntity;
import com.youjitech.system.base.mapper.BaseMapper;
import com.youjitech.system.role.realms.ShiroRDbRealm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/15
 */
public class BaseService<M extends NoIdxEntity, ID> {

    @Autowired
    private BaseMapper<M, ID> baseMapper;

    public void add(M m) {
        Subject subject = SecurityUtils.getSubject();
        if(subject != null){
            ShiroRDbRealm.ShiroUser user = (ShiroRDbRealm.ShiroUser)subject.getPrincipal();
            m.setAamIdx(user.getIdx());
//            m.setAmCode(user.getAmCode());
        }
        baseMapper.add(m);
    }

    public void update(M m) {
        baseMapper.update(m);
    }

    public void delete(ID id) {
        baseMapper.delete(id);
    }

    public M findByIdx(ID id) {
        return baseMapper.findByIdx(id);
    }

    public Page<M> findByCondition(Map<String, Object> params) {
        PageUtil.initPage(params);
        return baseMapper.findByCondition(params);
    }

    public List<M> findAll(){
        return baseMapper.findByCondition(Maps.newHashMap());
    }

    public List<M> findList(M m) {
        return baseMapper.findList(m);
    }

    public M getByCondition(M m) {
        return baseMapper.getByCondition(m);
    }

}
