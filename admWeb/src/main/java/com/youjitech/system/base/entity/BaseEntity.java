package com.youjitech.system.base.entity;

import lombok.Data;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Data
public class BaseEntity extends NoIdxEntity {

    protected Integer idx;

}
