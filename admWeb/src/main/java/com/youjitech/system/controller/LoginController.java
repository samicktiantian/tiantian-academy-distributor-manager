package com.youjitech.system.controller;

import com.youjitech.common.exception.ExceptionUtil;
import com.youjitech.system.base.controller.BaseController;
import com.youjitech.system.role.realms.ShiroRDbRealm.ShiroUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/16
 */
@Controller
public class LoginController extends BaseController {


    @RequestMapping("")
    public String toLogin() {
        return "login";
    }

    @RequestMapping("login")
    public String login(Model model, HttpServletRequest request){

        //判断用户是否已经登录
        if(SecurityUtils.getSubject().isAuthenticated()){
            return	"redirect:/index";
        }
        return "login";
    }

    @RequestMapping("logout")
    public String logout(){

        //判断用户是否已经登录
        if(SecurityUtils.getSubject().isAuthenticated()){
            SecurityUtils.getSubject().logout();
        }
        return "login";
    }

    @RequestMapping(value="/webLogin")
    public ResponseEntity<?> webLogin(@RequestBody Map<String, Object> params,
        HttpSession session, HttpServletRequest request, HttpServletResponse response){

        Subject subject = SecurityUtils.getSubject();
        if(subject.isAuthenticated()) {
            SecurityUtils.getSubject().logout();
			/*//已经登陆过
			resultMap = ControllerUtil.getSuccessCode();*/
        }

        String loginName = (String)params.get("loginName");
        String password = (String)params.get("password");
        String validCode = (String)params.get("validCode");

        //判断验证码是否正确
        String sessionValidCode = (String)session.getAttribute("rand");
        if(sessionValidCode == null || !sessionValidCode.equalsIgnoreCase(validCode)){
            ExceptionUtil.checkException("exception.validate.code.error");
        }

        UsernamePasswordToken token = new UsernamePasswordToken(loginName, password.toCharArray());
        subject.login(token);

        //TODO 测试完需要删除
        //暂时设置登录不超时
//		subject.getSession().setTimeout(-1000l);
        //暂时设置登录不超时
//        Subject subject = SecurityUtils.getSubject();
        //设置语言
        SessionLocaleResolver localeResolver = (SessionLocaleResolver)RequestContextUtils.getLocaleResolver(request);
        ShiroUser user = (ShiroUser)subject.getPrincipal();

        Locale locale = new Locale(user.getLang());
        localeResolver.setLocale(request, response, locale);

        //将语言设置到session中
        session.setAttribute("lang", user.getLang());

        //设置时差
        Integer timezone = Integer.valueOf(request.getParameter("timezone"));
//        session.setAttribute("TZ_OFFSET", timezone);
        user.setTzOffset(timezone);

        Map<String, Object> resultMap = getSuccessMap();
        resultMap.put("user", getCurrentUser());

        return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
    }

    @RequestMapping(value="index")
    public String index() {
        return "index";
    }

    @RequestMapping(value="dashboard")
    public String dashboard() {
        return "dashboard";
    }
}
