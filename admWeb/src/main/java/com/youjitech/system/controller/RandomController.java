package com.youjitech.system.controller;

import com.youjitech.common.util.EncryptUtil;
import com.youjitech.common.util.ExportUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class RandomController {
	
	/**
	 * 生成四位随机码作为sessionKey
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return null
	 * @throws IOException
	 */
	@RequestMapping("/randomNo")
    public void ramdon(final HttpServletRequest request, final HttpServletResponse response,final HttpSession session) throws IOException  {
		final int length = getParam(request,"length",4);
		final int width = getParam(request,"width",26);
		final int height = getParam(request,"height",34);
		final int padding = getParam(request,"padding",3);
		
		final String ramdonNo = EncryptUtil.getRandomString(length);
		session.setAttribute("rand", ramdonNo);

		ExportUtil.exportRamdonNo(ramdonNo,width,height,padding,null,response);
    }
	
	private int getParam(final HttpServletRequest request, final String paramName , final int defValue){
		final String param = StringUtils.trim(request.getParameter(paramName));
		final int rtn;
		if(StringUtils.isEmpty(param)){
			rtn = defValue;
		}else{
			rtn = Integer.parseInt(param);
		}
		return rtn;
	}
}
