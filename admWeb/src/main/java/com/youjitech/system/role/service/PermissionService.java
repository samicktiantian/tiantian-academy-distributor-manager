package com.youjitech.system.role.service;

import com.youjitech.system.base.service.BaseService;
import com.youjitech.system.role.entity.Permission;
import com.youjitech.system.role.mapper.PermissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Service
public class PermissionService extends BaseService<Permission, Integer> {

    @Autowired
    private PermissionMapper permissionMapper;
}
