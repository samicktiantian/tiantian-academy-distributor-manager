package com.youjitech.system.role.service;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.youjitech.common.Constant;
import com.youjitech.common.exception.ExceptionUtil;
import com.youjitech.system.base.service.BaseService;
import com.youjitech.system.role.entity.Permission;
import com.youjitech.system.role.entity.Resource;
import com.youjitech.system.role.entity.Role;
import com.youjitech.system.role.entity.RoleResourcePermission;
import com.youjitech.system.role.mapper.*;
import com.youjitech.system.user.entity.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Service
public class RoleService extends BaseService<Role, Integer> {

    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private ResourceMapper resourceMapper;
    @Autowired
    private RoleResourcePermissionMapper roleResourcePermissionMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;

    public Set<String> findStringRoles(User user) {

        List<Integer> roleIdList = userRoleMapper.findRoleIdxByUserIdx(user.getIdx());

        Set<String> roleNameSet = Sets.newHashSet();

        for (Integer roleId : roleIdList) {
            Role role = roleMapper.findByIdx(roleId);
            roleNameSet.add(role.getIdentity());
        }
        return roleNameSet;

    }

    @Transactional
    public void updateRoleResourcePermission(Role role) {
        //删除原来的权限记录
        deleteOldRoleResourcePermissions(role.getIdx());

        //封装资源
        saveRoleResourcePermission(role);
    }


    private void saveRoleResourcePermission(Role role) {
        if(role.getPermissionIds().size() == 0){
            return;
        }

        Map<Integer, String> resourcePermissionMap = Maps.newHashMap();
        for(Integer permissionIdx : role.getPermissionIds()){
            Permission permission = permissionMapper.findByIdx(permissionIdx);
            Resource resource = resourceMapper.findByIdx(permission.getResourceIdx());

            if(resource.getParentId() != null){
                Resource parentResource = resourceMapper.findByIdx(resource.getParentId());
                if(!resourcePermissionMap.containsKey(parentResource.getIdx())){
                    wrapResourcePermission(resourcePermissionMap, parentResource.getIdx());
                }
            }
            String permissionStr = resourcePermissionMap.get(resource.getIdx());
            resourcePermissionMap.put(resource.getIdx(), StringUtils.isEmpty(permissionStr) ? permission.getPermission() : permissionStr + ";" + permission.getPermission());
        }

        //加入顶级菜单资源（默认做2级菜单，将来如果有其他需要，再另行改造）
        if(!resourcePermissionMap.isEmpty()) {
            Resource topResource = resourceMapper.findByResourceLevel(Constant.DEFAULT_RESOURCE_AVAILABLE_LEVEL);
            wrapResourcePermission(resourcePermissionMap, topResource.getIdx());
        }

        Role originalRole = roleMapper.findByIdx(role.getIdx());
        for (Map.Entry<Integer, String> entry: resourcePermissionMap.entrySet()) {
            RoleResourcePermission rrp = new RoleResourcePermission();
            rrp.setRoleIdx(originalRole.getIdx());
            rrp.setResourceIdx(entry.getKey());
            rrp.setPermissionString(entry.getValue());
            roleResourcePermissionMapper.add(rrp);
        }
    }

    private void deleteOldRoleResourcePermissions(Integer roleIdx) throws RuntimeException {

//		List<RoleResourcePermission> rrpList = roleResourcePermissionMapper.findByRoleId(roleId);

        roleResourcePermissionMapper.deleteByRoleIdx(roleIdx);
    }

    private void wrapResourcePermission(Map<Integer, String> resourcePermissionMap, Integer resourceIdx){
        List<Permission> permissionList = permissionMapper.findByResourceIdx(resourceIdx);
        resourcePermissionMap.put(resourceIdx, convertPermissionToStr(permissionList));
    }

    private String convertPermissionToStr(List<Permission> permissionList) {
        String permissionStr = "";
        for (Permission permission : permissionList) {
            permissionStr += StringUtils.isEmpty(permissionStr) ? permission.getPermission() : permissionStr + ";" + permission.getPermission();
        }
        return permissionStr;
    }

    public void addRole(Role role) {
        validateRoleName(role.getRoleName());
        roleMapper.add(role);
    }

    public void validateRoleName(String roleName){
        Role condition = new Role();
        condition.setRoleName(roleName);

        Role originalData = roleMapper.getByCondition(condition);
        if(originalData != null) {
            ExceptionUtil.checkException("exception.role.exists");
        }
    }

    @Override
    @Transactional
    public void delete(Integer roleIdx) {
        //delete role
        roleMapper.delete(roleIdx);
        //delete user role
        userRoleMapper.deleteByRoleIdx(roleIdx);
        //delete user resource permission
        roleResourcePermissionMapper.deleteByRoleIdx(roleIdx);
    }
}
