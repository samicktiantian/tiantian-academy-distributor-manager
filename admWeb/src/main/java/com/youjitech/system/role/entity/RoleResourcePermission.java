package com.youjitech.system.role.entity;

import com.youjitech.system.base.entity.BaseEntity;
import lombok.Data;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Data
public class RoleResourcePermission extends BaseEntity {

    private Integer roleIdx;

    private Integer resourceIdx;

    private String permissionString;
}
