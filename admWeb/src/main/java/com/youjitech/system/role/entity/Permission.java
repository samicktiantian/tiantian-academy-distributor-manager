package com.youjitech.system.role.entity;

import com.youjitech.system.base.entity.BaseEntity;
import lombok.Data;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Data
public class Permission extends BaseEntity {

    private Integer pIdx;
    private String name;
    private String permission;
    private String description;
    private Integer isShow;
    private Integer resourceIdx;

    private Integer permitFlag = 0;

}
