package com.youjitech.system.role.filter;

import com.youjitech.common.Constant;
import com.youjitech.common.util.WebUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * Created By wangbo
 * 2019/1/15
 */
public class RoleAuthorizationFilter extends AuthorizationFilter {


    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        Subject subject = getSubject(request, response);

        if(subject.getPrincipal() == null) {
            if(WebUtil.isAjax(httpRequest)) {
                WebUtil.sendJson(httpResponse, Constant.UNLOGIN_CODE, "NOT LOGIN IN");
            } else {
                saveRequestAndRedirectToLogin(request, response);
            }
        }else {
            if(WebUtil.isAjax(httpRequest)) {
                WebUtil.sendJson(httpResponse, Constant.NO_PERMISSION_CODE, "NO PERMISSION");
            }else {
                String unauthorizedUrl = getUnauthorizedUrl();
                if(!StringUtils.isBlank(unauthorizedUrl)) {
                    WebUtils.issueRedirect(request, response, unauthorizedUrl);
                } else {
                    WebUtils.toHttp(response).sendError(401);
                }
            }
        }

        return false;
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue)
            throws Exception {

        Subject subject = getSubject(request, response);

        if(subject.getPrincipal() == null && !subject.isAuthenticated()) {
            //没有令牌或者未授权,返回false
            return false;
        }

        String[] rolesArray = (String[]) mappedValue;

        if (rolesArray == null || rolesArray.length == 0) {
            // no roles specified, so nothing to check - allow access.
            return true;
        }

        Set<String> roles = CollectionUtils.asSet(rolesArray);
        for (String role : roles) {
            if (subject.hasRole(role)) {
                return true;
            }
        }

        return false;
    }
}
