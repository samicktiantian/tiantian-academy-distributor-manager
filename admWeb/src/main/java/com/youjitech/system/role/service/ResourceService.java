package com.youjitech.system.role.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.youjitech.common.Constant;
import com.youjitech.system.base.service.BaseService;
import com.youjitech.system.menu.vo.Menu;
import com.youjitech.system.role.entity.Permission;
import com.youjitech.system.role.entity.Resource;
import com.youjitech.system.role.entity.RoleResourcePermission;
import com.youjitech.system.role.mapper.*;
import com.youjitech.system.user.entity.User;
import com.youjitech.system.user.mapper.UserMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.dozer.util.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Service
public class ResourceService extends BaseService<Resource, Integer> {

    @Autowired
    private ResourceMapper resourceMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private RoleResourcePermissionMapper roleResourcePermissionMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleMapper roleMapper;


    public Set<String> findStringPermissions(Integer userIdx) {
        Set<String> permissions = Sets.newHashSet();

        List<Integer> roleIds = userRoleMapper.findRoleIdxByUserIdx(userIdx);

        for (Integer roleIdx : roleIds) {

            List<RoleResourcePermission> roleResourcePermissionList = roleResourcePermissionMapper.findByRoleIdx(roleIdx);

            for (RoleResourcePermission rrp : roleResourcePermissionList) {
                Resource resource = resourceMapper.findByIdx(rrp.getResourceIdx());
                if (resource == null) {
                    continue;
                }
                if (resource.getResourceLevel() <= Constant.DEFAULT_RESOURCE_AVAILABLE_LEVEL) {
                    // 默认过滤掉1,级菜单
                    continue;
                }
                if (rrp.getPermissionString().indexOf(Constant.DEFAULT_BASIC_AVAILABLE_PERMISSION) == -1) {
                    // 如果对资源没有查看权限,则不显示菜单
                    continue;
                }

                String actualResourceIdentity = findActualResourceIdentity(resource);

                // 不可用 即没查到 或者标识字符串不存在
                if (resource == null || StringUtils.isEmpty(actualResourceIdentity)
                        || Constant.FLAG_STRING_NO == resource.getIsShow()) {
                    continue;
                }
                for (String permissionString : rrp.getPermissionString().split(";")) {
                    Permission permission = permissionMapper.findByPermissionAndResourceIdx(permissionString, resource.getIdx());

                    // 不可用
                    if (permission == null || Constant.FLAG_INT_NO == permission.getIsShow()) {
                        continue;
                    }
                    permissions.add(actualResourceIdentity + ":" + permission.getPermission());
                }
            }
        }

        return permissions;
    }


    public String findActualResourceIdentity(Resource resource) {
        if (resource == null || StringUtils.isEmpty(resource.getIdentity())) {
            return null;
        }

        StringBuilder s = new StringBuilder(resource.getIdentity());

        // boolean hasResourceIdentity = !StringUtils.isEmpty(resource
        // .getIdentity());
        //
        // Resource parent = findOne(resource.getParentId());
        //
        // while (parent != null) {
        // if (!StringUtils.isEmpty(parent.getIdentity())) {
        // s.insert(0, parent.getIdentity() + ":");
        // hasResourceIdentity = true;
        // }
        // parent = findOne(parent.getParentId());
        // }
        //
        // // 如果用户没有声明 资源标识 且父也没有，那么就为空
        // if (!hasResourceIdentity) {
        // return "";
        // }
        //
        // // 如果最后一个字符是: 因为不需要，所以删除之
        // int length = s.length();
        // if (length > 0 && s.lastIndexOf(":") == length - 1) {
        // s.deleteCharAt(length - 1);
        // }
        //
        // // 如果有儿子 最后拼一个*
        // boolean hasChildren = false;
        // for (Resource r : findAll()) {
        // if (resource.getId().equals(r.getParentId())) {
        // hasChildren = true;
        // break;
        // }
        // }
        // if (hasChildren) {
        // s.append(":*");
        // }

        return s.toString();
    }

    public List<Menu> findMenus(Integer userId) {

        User user = userMapper.findByIdx(userId);

        List<Resource> resourceList = resourceMapper.findAllOrderByWeightDesc();

        Set<String> userPermissions = findStringPermissions(user.getIdx());

        Iterator<Resource> iter = resourceList.iterator();
        while (iter.hasNext()) {
            Resource resource = iter.next();
            if (!hasPermission(resource, userPermissions)) {
                iter.remove();
            }
        }
        return convertToMenus(IteratorUtils.toList(resourceList.iterator()));
    }

    public static List<Menu> convertToMenus(List<Resource> resources) {
        if (resources.size() == 0) {
            return Collections.emptyList();
        }

        Menu root = convertToMenu(resources.remove(resources.size() - 1));

        recursiveMenu(root, resources);
        List<Menu> menus = root.getChildren();
        removeNoLeafMenu(menus);

        return menus;
    }

    private static void removeNoLeafMenu(List<Menu> menus) {
        if (menus.size() == 0) {
            return;
        }
        for (int i = menus.size() - 1; i >= 0; i--) {
            Menu m = menus.get(i);
            removeNoLeafMenu(m.getChildren());
            if (!m.isHasChildren() && StringUtils.isEmpty(m.getUrl())) {
                menus.remove(i);
            }
        }
    }

    private static void recursiveMenu(Menu menu, List<Resource> resources) {
        for (int i = resources.size() - 1; i >= 0; i--) {
            Resource resource = resources.get(i);
            if (resource.getParentId().equals(menu.getIdx())) {
                menu.getChildren().add(convertToMenu(resource));
                resources.remove(i);
            }
        }

        for (Menu subMenu : menu.getChildren()) {
            recursiveMenu(subMenu, resources);
        }
    }

    private static Menu convertToMenu(Resource resource) {

        Map<String, String> menuNames = Maps.newHashMap();
        menuNames.put(Constant.LANG_EN, resource.getName());
        menuNames.put(Constant.LANG_ZH_CN, resource.getNameCn());
        menuNames.put(Constant.LANG_KO, resource.getNameKo());

        return new Menu(resource.getIdx(), resource.getName(), resource.getUrl(), resource.getIconPath(),
                resource.getIdentity(), resource.getResourceLevel(), menuNames);
    }

    private boolean hasPermission(Resource resource, Set<String> userPermissions) {
        String actualResourceIdentity = findActualResourceIdentity(resource);
        if (StringUtils.isEmpty(actualResourceIdentity)) {
            return true;
        }

        for (String permission : userPermissions) {
            if (hasPermission(permission, actualResourceIdentity)) {
                return true;
            }
        }

        return false;
    }

    private boolean hasPermission(String permission, String actualResourceIdentity) {

        // 得到权限字符串中的 资源部分，如a:b:create --->资源是a:b
        String permissionResourceIdentity = permission.substring(0, permission.lastIndexOf(":"));

        // 如果权限字符串中的资源 是 以资源为前缀 则有权限 如a:b 具有a:b的权限
        if (permissionResourceIdentity.startsWith(actualResourceIdentity)) {
            return true;
        }

        // 模式匹配
        WildcardPermission p1 = new WildcardPermission(permissionResourceIdentity);
        WildcardPermission p2 = new WildcardPermission(actualResourceIdentity);

        return p1.implies(p2) || p2.implies(p1);
    }

    public List<Resource> loadRolePermissions(Integer roleIdx) {
        List<Resource> finalResourceList = Lists.newArrayList();

        Set<Integer> permissionIds = convertPermissionIds(roleResourcePermissionMapper.findByRoleIdx(roleIdx));

        List<Resource> allVisiableResource = findAllVisiableResource();

        for (Resource resource : allVisiableResource) {

            List<Permission> permissionList = permissionMapper.findByResourceIdx(resource.getIdx());

            for (Permission permission : permissionList) {
                for (Integer permissionIdx : permissionIds) {
                    if (permissionIdx.intValue() == permission.getIdx().intValue()) {
                        permission.setPermitFlag(Constant.FLAG_INT_YES);
                    }
                }
                resource.getPermissionList().add(permission);
            }
            finalResourceList.add(resource);
        }

        return finalResourceList;
    }

    private List<Resource> findAllVisiableResource() {
        Map<String, Object> searchParams = Maps.newHashMap();

        searchParams.put("resourceLevel", 3);
        searchParams.put("isShow", Constant.FLAG_INT_YES);
        return resourceMapper.findByCondition(searchParams);
    }

    private Set<Integer> convertPermissionIds(List<RoleResourcePermission> resourcePermissions) {

        Set<Integer> permissionIds = Sets.newHashSet();

        for (RoleResourcePermission resourcePermission : resourcePermissions) {
            String[] permissions = resourcePermission.getPermissionString().split(";");
            for (String permission : permissions) {
                Permission p = permissionMapper.findByPermissionAndResourceIdx(permission, resourcePermission.getResourceIdx());
                if (p != null) {
                    permissionIds.add(p.getIdx());
                }
            }
        }
        return permissionIds;
    }
}
