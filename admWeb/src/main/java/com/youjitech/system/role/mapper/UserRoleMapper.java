package com.youjitech.system.role.mapper;

import com.youjitech.system.base.mapper.BaseMapper;
import com.youjitech.system.role.entity.UserRole;

import java.util.List;

/**
 * Created By wangbo
 * 2019/1/15
 */
public interface UserRoleMapper extends BaseMapper<UserRole, Integer> {

    List<Integer> findRoleIdxByUserIdx(Integer userIdx);

    void deleteByRoleIdx(Integer roleIdx);

    void deleteByUserIdx(Integer uIdx);
}
