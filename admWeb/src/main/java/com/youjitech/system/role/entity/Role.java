package com.youjitech.system.role.entity;

import com.youjitech.system.base.entity.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Data
public class Role extends BaseEntity {

    private Integer idx;
    private String roleName;
    private String identity;
    private String description;

    List<Integer> permissionIds;
}
