package com.youjitech.system.role.entity;

import com.youjitech.system.base.entity.BaseEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Data
public class Resource extends BaseEntity {

    private String name;
    private String nameCn;
    private String nameKo;

    private String identity;
    private String url;
    private Integer parentId;
    private String parentIds;
    private Integer weight;
    private String isShow;
    private String iconPath;
    private Integer resourceLevel;
    private String permissionName;
    private String parentName;

    private List<Permission> permissionList = new ArrayList<Permission>();
}
