package com.youjitech.system.role.mapper;

import com.youjitech.system.base.mapper.BaseMapper;
import com.youjitech.system.role.entity.Role;

/**
 * Created By wangbo
 * 2019/1/15
 */
public interface RoleMapper extends BaseMapper<Role, Integer> {
}
