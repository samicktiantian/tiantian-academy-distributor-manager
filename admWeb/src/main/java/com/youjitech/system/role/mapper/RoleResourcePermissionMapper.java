package com.youjitech.system.role.mapper;

import com.youjitech.system.base.mapper.BaseMapper;
import com.youjitech.system.role.entity.RoleResourcePermission;

import java.util.List;

/**
 * Created By wangbo
 * 2019/1/15
 */
public interface RoleResourcePermissionMapper extends BaseMapper<RoleResourcePermission, Integer> {

    List<RoleResourcePermission> findByRoleIdx(Integer roleIdx);

    void deleteByRoleIdx(Integer roleIdx);
}
