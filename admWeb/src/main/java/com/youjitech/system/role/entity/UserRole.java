package com.youjitech.system.role.entity;

import com.youjitech.system.base.entity.BaseEntity;
import lombok.Data;

/**
 * Created By wangbo
 * 2019/1/15
 */
@Data
public class UserRole extends BaseEntity {

    private Integer userIdx;
    private Integer roleIdx;

}
