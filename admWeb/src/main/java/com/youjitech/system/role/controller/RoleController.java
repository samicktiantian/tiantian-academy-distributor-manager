package com.youjitech.system.role.controller;

import com.google.common.collect.Maps;
import com.youjitech.system.base.controller.BaseFunctionController;
import com.youjitech.system.role.entity.Role;
import com.youjitech.system.role.service.ResourceService;
import com.youjitech.system.role.service.RoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/18
 */
@Controller
@RequestMapping(value = "/role")
public class RoleController extends BaseFunctionController<Role, Integer> {

    @Autowired
    private RoleService roleService;

    @Autowired
    private ResourceService resourceService;

    @RequiresPermissions(value = "role:auth")
    @GetMapping(value = "/permissionList/{roleIdx}")
    public ModelAndView permissionList(@PathVariable Integer roleIdx) {
        Map<String, Object> result = Maps.newHashMap();
        result.put("rolePermissionList", resourceService.loadRolePermissions(roleIdx));
        result.put("roleIdx", roleIdx);
        return generateModelAndView(result, toPage("permissionList"));
    }

    @RequiresPermissions(value = "role:auth")
    @PostMapping(value = "/setRoleResourcePermission")
    public ResponseEntity<?> setRoleResourcePermission(@RequestBody Role role) {
        roleService.updateRoleResourcePermission(role);
        return new ResponseEntity<>(getSuccessMap(), HttpStatus.OK);
    }

    @RequiresPermissions(value = "role:add")
    @PostMapping(value = "/add")
    public ResponseEntity<?> add(Role role) {
        roleService.addRole(role);
        return new ResponseEntity<>(getSuccessCode(), HttpStatus.OK);
    }

    @RequiresPermissions(value = "role:delete")
    @GetMapping(value = "/delete/{roleIdx}")
    public ResponseEntity<?> delRole(@PathVariable Integer roleIdx) {
        roleService.delete(roleIdx);
        return new ResponseEntity<>(getSuccessMap(), HttpStatus.OK);
    }

}
