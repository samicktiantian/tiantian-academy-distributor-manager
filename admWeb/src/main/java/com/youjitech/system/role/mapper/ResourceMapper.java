package com.youjitech.system.role.mapper;

import com.youjitech.system.base.mapper.BaseMapper;
import com.youjitech.system.role.entity.Resource;

import java.util.List;

/**
 * Created By wangbo
 * 2019/1/15
 */
public interface ResourceMapper extends BaseMapper<Resource, Integer> {

    List<Resource> findAllOrderByWeightDesc();


    Resource findByResourceLevel(int defaultResourceAvailableLevel);
}
