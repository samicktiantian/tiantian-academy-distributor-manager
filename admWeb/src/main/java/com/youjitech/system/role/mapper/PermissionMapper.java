package com.youjitech.system.role.mapper;

import com.youjitech.system.base.mapper.BaseMapper;
import com.youjitech.system.role.entity.Permission;

import java.util.List;

/**
 * Created By wangbo
 * 2019/1/15
 */
public interface PermissionMapper extends BaseMapper<Permission, Integer> {

    Permission findByPermissionAndResourceIdx(String permissionString, Integer resourceIdx);

    List<Permission> findByResourceIdx(Integer resourceIdx);
}
