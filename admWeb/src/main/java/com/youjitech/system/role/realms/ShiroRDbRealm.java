package com.youjitech.system.role.realms;

import com.youjitech.common.Constant;
import com.youjitech.common.exception.NoPermissionException;
import com.youjitech.system.role.service.ResourceService;
import com.youjitech.system.role.service.RoleService;
import com.youjitech.system.user.entity.User;
import com.youjitech.system.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created By wangbo
 * 2019/1/16
 */
public class ShiroRDbRealm extends AuthorizingRealm {

    public static final String HASH_ALGORITHM = "SHA-1";

    public static final int HASH_INTERATIONS = 1024;

    public static final int SALT_SIZE = 8;

    protected UserService userService;

    protected ResourceService resourceService;

    protected RoleService roleService;

    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        ShiroUser shiroUser = (ShiroUser) principals.getPrimaryPrincipal();
        User user = userService.findByLoginName(shiroUser.loginName);

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setRoles(roleService.findStringRoles(user));
        authorizationInfo.setStringPermissions(resourceService.findStringPermissions(user.getIdx()));

        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
        String username = token.getUsername().trim();
        User user = userService.findByLoginName(username);

        if (user != null) {
            if (!Constant.USER_STATUS_NORMAL.equals(user.getStatus())) {
                throw new DisabledAccountException();
            }

            //判断用户是否有可查看的权限
            boolean permissionFlag = userService.validateUserResourcePermission(user);
            if (!permissionFlag) {
                throw new NoPermissionException();
            }
//            byte[] salt = Encodes.decodeHex(user.getSalt());
            return new SimpleAuthenticationInfo(new ShiroUser(user.getIdx(), user.getLoginName(), user.getRealName(), user.getLanguage()/*, user.getAmCode()*/), user.getPassword(), getName());
        } else {
            throw new UnknownAccountException();
        }
    }

    public static void clearAuthenticationInfoCache() {
        //清除认证缓存
        Subject subject = SecurityUtils.getSubject();
        RealmSecurityManager securityManager = (RealmSecurityManager) SecurityUtils.getSecurityManager();
        ShiroRDbRealm userRealm = (ShiroRDbRealm) securityManager.getRealms().iterator().next();
        userRealm.clearCachedAuthenticationInfo(subject.getPrincipals());
    }

    public static void clearAuthorizationInfoCache() {
        //清除授权缓存
        Subject subject = SecurityUtils.getSubject();
        RealmSecurityManager securityManager = (RealmSecurityManager) SecurityUtils.getSecurityManager();
        ShiroRDbRealm userRealm = (ShiroRDbRealm) securityManager.getRealms().iterator().next();
        userRealm.clearCachedAuthorizationInfo(subject.getPrincipals());
    }

    /**
     * 设定Password校验的Hash算法与迭代次数.
     */
    @PostConstruct
    public void initCredentialsMatcher() {
//        HashedCredentialsMatcher matcher = new HashedCredentialsMatcher(HASH_ALGORITHM);
//        matcher.setHashIterations(HASH_INTERATIONS);
//        setCredentialsMatcher(matcher);

        setCredentialsMatcher(new CustomCredentialsMatcher());
    }

    private static final String OR_OPERATOR = " or ";
    private static final String AND_OPERATOR = " and ";
    private static final String NOT_OPERATOR = "not ";


    /**
     * 支持or and not 关键词  不支持and or混用
     *
     * @param principals
     * @param permission
     * @return
     */
    public boolean isPermitted(PrincipalCollection principals, String permission) {
        if (permission.contains(OR_OPERATOR)) {
            String[] permissions = permission.split(OR_OPERATOR);
            for (String orPermission : permissions) {
                if (isPermittedWithNotOperator(principals, orPermission)) {
                    return true;
                }
            }
            return false;
        } else if (permission.contains(AND_OPERATOR)) {
            String[] permissions = permission.split(AND_OPERATOR);
            for (String orPermission : permissions) {
                if (!isPermittedWithNotOperator(principals, orPermission)) {
                    return false;
                }
            }
            return true;
        } else {
            return isPermittedWithNotOperator(principals, permission);
        }
    }

    private boolean isPermittedWithNotOperator(PrincipalCollection principals, String permission) {
        if (permission.startsWith(NOT_OPERATOR)) {
            return !super.isPermitted(principals, permission.substring(NOT_OPERATOR.length()));
        } else {
            return super.isPermitted(principals, permission);
        }
    }

    public static class ShiroUser implements Serializable {
        private static final long serialVersionUID = -1373760761780840081L;
        private Integer idx;
        private String loginName;
        private String userName;
        private String lang;
//        private Integer amCode;
        private Integer tzOffset;

        public ShiroUser(Integer idx, String loginName, String userName, String lang/*, Integer amCode*/) {
            this.idx = idx;
            this.loginName = loginName;
            this.userName = userName;
            this.lang = lang;
//            this.amCode = amCode;
        }

//        public Integer getAmCode() {
//            return amCode;
//        }

        public Integer getTzOffset() {
            return tzOffset;
        }

        public void setTzOffset(Integer tzOffset) {
            this.tzOffset = tzOffset;
        }

        public Integer getIdx() {
            return idx;
        }

        public String getUserName() {
            return userName;
        }

        public String getLang() {
            return StringUtils.isEmpty(lang) ? "zh_CN" : lang;
        }

        /**
         * 本函数输出将作为默认的<shiro:principal/>输出.
         */
        @Override
        public String toString() {
            return loginName;
        }

        /**
         * 重载hashCode,只计算loginName;
         */
        @Override
        public int hashCode() {
            return Objects.hashCode(loginName);
        }

        /**
         * 重载equals,只计算loginName;
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            ShiroUser other = (ShiroUser) obj;
            if (loginName == null) {
                if (other.loginName != null) {
                    return false;
                }
            } else if (!loginName.equals(other.loginName)) {
                return false;
            }
            return true;
        }
    }
}
