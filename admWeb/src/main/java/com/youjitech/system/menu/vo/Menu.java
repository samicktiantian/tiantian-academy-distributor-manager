package com.youjitech.system.menu.vo;

import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created By wangbo
 * 2019/1/16
 */
@Data
public class Menu implements Serializable {

    public Menu(Integer idx, String name, String url, String menuIcon, String identity, Integer menuLevel, Map<String, String> menuNames) {
        super();
        this.idx = idx;
        this.name = name;
        this.url = url;
        this.menuIcon = menuIcon;
        this.identity = identity;
        this.menuLevel = menuLevel;
        this.menuNames = menuNames;
    }

    private Integer idx;
    private String name;
    private String url;
    private String menuIcon;
    private String identity;
    private Integer menuLevel;
    private List<Menu> children;
    private Map<String, String> menuNames;

    public List<Menu> getChildren() {
        if (children == null) {
            children = Lists.newArrayList();
        }
        return children;
    }

    public boolean isHasChildren() {
        return !getChildren().isEmpty();
    }
}
