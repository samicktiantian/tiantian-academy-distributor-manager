package com.youjitech.system.menu.controller;

import com.youjitech.common.Constant;
import com.youjitech.system.base.controller.BaseController;
import com.youjitech.system.menu.vo.Menu;
import com.youjitech.system.role.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created By wangbo
 * 2019/1/16
 */
@RestController
@RequestMapping(value = "/menu")
public class MenuController extends BaseController {

    @Autowired
    private ResourceService resourceService;

    @GetMapping(value = "/init")
    public ResponseEntity<?> initMenu(HttpSession session){
        List<Menu> menus = (List<Menu>)session.getAttribute(Constant.MENU_NAME_KEY);
        if(menus == null){
            menus = resourceService.findMenus(getCurrentUserId());
            session.setAttribute(Constant.MENU_NAME_KEY, menus);
        }
        return new ResponseEntity<>(menus, HttpStatus.OK);
    }
}
