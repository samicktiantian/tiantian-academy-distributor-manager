(function($){

    /**
	 * 创建弹出层
     */
    $.baseWindow = function(params) {
		var simpleWindow = {};

		//var buttons = createButtons(params);

        var windowId = params.windowId || 'dw';

        var windowDiv = $('#'+windowId);
        if(windowDiv.length == 0) {
            $(document.body).append('<div id="'+windowId+'"></div>');
            windowDiv = $('#'+windowId);
        }

        var baseParams = {
            title : params.title,
            loadingMessage : 'LOADING........',
            width : params.width?params.width:400,
            height : params.height?params.height:400,
            closed : false,
            cache : false,
            modal : params.modal || false,
            //buttons : buttons,
            minimizable: params.minimizable || false,
            maximizable: params.maximizable || true,
            collapsible: params.collapsible || false,
            shadow: params.shadow || false,
			left: params.left || null,
			top: params.top || null,
            onClose : function(){
                if(params.closeFunction) {
                    params.closeFunction();
                }
                simpleWindow.dialog('destroy');
            }
        }

        if(params.dialogModel == true) {
            baseParams.content = "<iframe scrolling='auto' frameborder='0' src='"+params.url+"' style='width:100%; height:100%; display:block;'></iframe>";
        }else{
            baseParams.href = params.url;
        }

        simpleWindow = windowDiv.window(baseParams);
	}

    /**
     * 关闭弹出层
     */
    $.closeWindow = function(obj){
        obj.window('destroy');
    }

	/**
	 * 创建弹出层 模态框
	 */
	$.baseDialog = function(params){

		var simpleDialog = {};

        var buttons = new Array();

		if(params.dialogModel && params.dialogModel == true) {
			//dialog模式,内容以iframe模式打开
		}else{
			//如果参数要求隐藏保存按钮，则不添加保存按钮
            if(params.showOkBtn == undefined || params.showOkBtn == true){
                buttons.push(
                    {
                        iconCls : 'icon-save',
                        text : 'SAVE',
                        id : 'save-btn',
                        handler : function() {
                            params.okFunction(simpleDialog);
                        }
                    }
                );

                //增加关闭按钮
                buttons.push(
                    {
                        text : 'CLOSE',
                        iconCls : 'icon-cancel',
                        handler : function() {
                            if(params.cancelFunction){
                                params.cancelFunction();
                            }
                            simpleDialog.dialog('destroy');
                        }
                    }
                );
            }

            //判断是否有其他的button
            if(params.otherButton && params.otherButton.length > 0){
                $.each(params.otherButton, function(index, value){
                    buttons.push(value);
                });
            }
		}

		var dialogId = params.dialogId||'dd';

		var dialogDiv = $('#'+dialogId);
		if(dialogDiv.length == 0) {
			$(document.body).append('<div id="'+dialogId+'"></div>');
			dialogDiv = $('#'+dialogId);
		}

		var baseParams = {
            title : params.title,
            loadingMessage : 'LOADING........',
            width : params.width?params.width:400,
            height : params.height?params.height:400,
            closed : false,
            cache : false,
            modal : params.modal || true,
            buttons : buttons,
            minimizable: params.minimizable || false,
            maximizable: params.maximizable || true,
            collapsible: params.collapsible || false,
            shadow: params.shadow || false,
            onClose : function(){
                if(params.closeFunction) {
                    params.closeFunction();
                }
                simpleDialog.dialog('destroy');
            }
		}

		if(params.dialogModel == true) {
			baseParams.content = "<iframe scrolling='auto' frameborder='0' src='"+params.url+"' style='width:100%; height:100%; display:block;'></iframe>";
		}else{
            baseParams.href = params.url;
		}

		simpleDialog = $('#'+dialogId).dialog(baseParams);
	}

	/**
	 * 关闭弹出层
	 */
	$.closeDialog = function(obj){
		obj.dialog('destroy');
	}

	/**
	 * 创建数据表格
	 */
	$.baseDatagrid = function(params){
		var dataGridDivId = params.dgId?params.dgId:'dg'
		var datagridDiv = $('#' + dataGridDivId);
		if(datagridDiv.length == 0) {
			$(document.body).append('<div id="'+dataGridDivId+'"></div>');
			datagridDiv = $('#' + dataGridDivId);
		}

		return datagridDiv.datagrid({
		    url: params.url,
            frozenColumns : params.frozenColumns ? [params.frozenColumns] : undefined,
		    columns: params.columns ? [params.columns] : params.columnsArray ,
		    striped: true,
		    singleSelect: true,
		    loadMsg: 'LOADING......',
		    fitColumns: true,
		    rownumbers: true,
		    pageSize: params.pageSize || 20,
		    nowrap: params.nowrap == undefined ? true : params.nowrap,
		    selectOnCheck: params.selectOnCheck || true,
		    checkOnSelect: params.checkOnSelect || false,
		    toolbar: params.toolbarId || null,
		    pagination: params.showPage != undefined ?  params.showPage : true,
		    data : params.data,
            fit: params.fit == true ? true : undefined,
            queryParams : params.queryParams || {},
            pageList : params.pageList || [10, 20, 30, 40],
            onRowContextMenu : function(e, rowIndex, rowData){
                e.preventDefault(); // 阻止浏览器右键
		        if(params.rowContextMenu){
                    params.rowContextMenu(e, rowIndex, rowData);
                }
            },
            onClickCell: function(rowIndex, field, value){
		    	if(params.clickCell){
                    params.clickCell(rowIndex, field, value);
				}
			},
            onClickRow: function (rowIndex, rowData) {
                $(this).datagrid('unselectRow', rowIndex);
                if(params.clickRow){
                    params.clickRow(rowIndex, rowData);
                }
            },
		    onLoadSuccess: function(data){
		    	if(data.total == 0){
		    	    if(params.columns && params.columns.length > 0){
                        var firstColumName = params.columns[0].field;
                        var rowObj = {};
                        rowObj[firstColumName] = '<div style="text-align:center;color:#9C9595;"><b>NO DATA！</b></div>';
                        $(this).datagrid('appendRow', rowObj);
                        $(this).datagrid('mergeCells', { index: 0, field: firstColumName, colspan: (params.columns.length) });
                    }
				}

				if(params.loadSuccess){
		    		params.loadSuccess(data);
		    		if(params.bindEnter == undefined || params.bindEnter == true){
		    			//当页面加载完毕后，绑定回车事件
		    			$('#queryForm input').keypress(function(e) {
		    				if (e.which == 13) {
		    					$('#search').click();
		    				}
		    			});
		    		}
		    	}
		    },
            rowStyler:function(index, row){
		        if(params.rowStyler){
                    return params.rowStyler(index, row);
                }

            }
		});
	}

	//format date
    $.fn.datebox.defaults.formatter = function(date){
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        return y+'-'+m+'-'+d;
    }


})(jQuery);