﻿
$(function(){
	// initLeftMenu();
	// tabClose();
	// tabCloseEven();




	/*$('#tabs').tabs('add',{
		title:'dashboard',
		content:createFrame(baseUrl + '/dashboard')
	})*//*.tabs({
        onSelect: function (title) {
            var currTab = $('#tabs').tabs('getTab', title);
            var iframe = $(currTab.panel('options').content);

			var src = iframe.attr('src');
			if(src)
				$('#tabs').tabs('update', { tab: currTab, options: { content: createFrame(src)} });

        }
    });*/

})

//初始化左侧
function initLeftMenu() {

	

	$("#nav").accordion({animate:false});

	$.baseGet('/menu/init', function(data){
		$.each(data, function(i, n) {
			var menulist ='';
			menulist +='<ul>';
	        $.each(n.children, function(j, o) {
				menulist += '<li><div><a ref="'+o.idx+'" href="#" rel="' + o.url + '" ><span class="icon  ' +o.menuIcon+'" >&nbsp;</span><span class="nav">' + o.menuNames[lang] + '</span></a></div></li> ';
	        })
			menulist += '</ul>';

			$('#nav').accordion('add', {
	            title: n.menuNames[lang],
	            content: menulist,
	            iconCls: 'icon '+n.menuIcon,
                animate: false
	        });

	    });

		$('.easyui-accordion li a').click(function(){
			var tabTitle = $(this).children('.nav').text();

			var url = baseUrl + $(this).attr("rel");
			var menuid = $(this).attr("ref");
			var icon = getIcon(data, menuid);

			addTab(tabTitle,url,icon);
			$('.easyui-accordion li div').removeClass("selected");
			$(this).parent().addClass("selected");
		}).hover(function(){
			$(this).parent().addClass("hover");
		},function(){
			$(this).parent().removeClass("hover");
		});

		//选中第一个
		var panels = $('#nav').accordion('panels');
		var t = panels[0].panel('options').title;
	    $('#nav').accordion('select', t);
	});

    $('#tabs').tabs('add',{
        title:'Dashboard',
		fit : true,
        iconCls : 'icon icon-edit',
        content:createFrame(baseUrl + '/dashboard')
    }).tabs({
        onSelect: function (title) {
            var currTab = $('#tabs').tabs('getTab', title);
            var iframe = $(currTab.panel('options').content);

            // var src = iframe.attr('src');
            // if(src) {
            //     $('#tabs').tabs('update', { tab: currTab, options: { content: createFrame(src)} });
			// }

        }
    });
}
//获取左侧导航的图标
function getIcon(menuList, menuid){
	var icon = 'icon ';
	$.each(menuList, function(i, n) {
		 $.each(n.children, function(j, o) {
		 	if(o.idx==menuid){
				icon += o.menuIcon;
			}
		 })
	})

	return icon;
}

function addTab(subtitle , url, icon){
	if(!$('#tabs').tabs('exists',subtitle)){
		$('#tabs').tabs('add',{
			title:subtitle,
			content:createFrame(url),
			closable:true,
			icon:icon
		});
	}else{
		$('#tabs').tabs('select',subtitle);
		$('#mm-tabupdate').click();
	}
	tabClose();
}

function createFrame(url)
{
	var s = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
	return s;
}

function tabClose()
{
	/*双击关闭TAB选项卡*/
	$(".tabs-inner").dblclick(function(){
		var subtitle = $(this).children(".tabs-closable").text();
		$('#tabs').tabs('close',subtitle);
	})
	/*为选项卡绑定右键*/
	$(".tabs-inner").bind('contextmenu',function(e){
		$('#mm').menu('show', {
			left: e.pageX,
			top: e.pageY
		});

		var subtitle =$(this).children(".tabs-closable").text();

		$('#mm').data("currtab",subtitle);
		$('#tabs').tabs('select',subtitle);
		return false;
	});
}
//绑定右键菜单事件
function tabCloseEven()
{
	//刷新
	$('#mm-tabupdate').click(function(){
		var currTab = $('#tabs').tabs('getSelected');
		var url = $(currTab.panel('options').content).attr('src');
		$('#tabs').tabs('update',{
			tab:currTab,
			options:{
				content:createFrame(url)
			}
		})
	})
	//关闭当前
	$('#mm-tabclose').click(function(){
		var currtab_title = $('#mm').data("currtab");
		$('#tabs').tabs('close',currtab_title);
	})
	//全部关闭
	$('#mm-tabcloseall').click(function(){
		$('.tabs-inner span').each(function(i,n){
			if(n.innerText != 'Dashboard') {
				var t = $(n).text();
				$('#tabs').tabs('close',t);
			}
		});
	});
	//关闭除当前之外的TAB
	$('#mm-tabcloseother').click(function(){
		$('#mm-tabcloseright').click();
		$('#mm-tabcloseleft').click();
	});
	//关闭当前右侧的TAB
	$('#mm-tabcloseright').click(function(){
		var nextall = $('.tabs-selected').nextAll();
		if(nextall.length==0){
			return false;
		}
		nextall.each(function(i,n){
			var t=$('a:eq(0) span',$(n)).text();
			$('#tabs').tabs('close',t);
		});
		return false;
	});
	//关闭当前左侧的TAB
	$('#mm-tabcloseleft').click(function(){
		var prevall = $('.tabs-selected').prevAll();
		if(prevall.length==0){
//			alert('到头了，前边没有啦~~');
			return false;
		}
		prevall.each(function(i,n){
            if(n.innerText != 'Dashboard') {
                var t = $('a:eq(0) span', $(n)).text();
                $('#tabs').tabs('close', t);
            }
		});
		return false;
	});

	//退出
	$("#mm-exit").click(function(){
		$('#mm').menu('hide');
	})
}

function addParentTab(subtitle , url, icon){
    if(parent.$('#tabs').tabs('exists',subtitle)){
        parent.$('#tabs').tabs('close', subtitle);
    }/*else{
        parent.$('#tabs').tabs('select',subtitle);
        parent.$('#mm-tabupdate').click();
    }*/
    parent.$('#tabs').tabs('add',{
        title:subtitle,
        content:createFrame(url),
        closable:true,
        icon:icon
    });

}
