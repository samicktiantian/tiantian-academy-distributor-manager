(function($) {
	$.basePost = function(url, param, callback) {
        $.baseMessageProgress({msg : '<spring:message code="global.progress.title"/>'})
		$.ajax({
			url : baseUrl + url,
			contentType : 'application/json',
			dataType : "json",
			type : "post",
			data : JSON.stringify(param),
			success : function(data) {
                $.baseMessageProgressClose();
				callback(data);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				handleResult(XMLHttpRequest);
			}
		});
	}
	
	$.baseGet = function(url, callback) {
        $.baseMessageProgress({msg : '<spring:message code="global.progress.title"/>'})
		$.ajax({
			url : baseUrl + url,
			contentType : 'application/json',
			dataType : "json",
			type : "get",
			success : function(data) {
                $.baseMessageProgressClose();
				callback(data);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				handleResult(XMLHttpRequest);
			}
		});
	}

	$.basePostNotAsync = function(url, param, callback) {
        $.baseMessageProgress({msg : '<spring:message code="global.progress.title"/>'})
		$.ajax({
			url : baseUrl + url,
			contentType : 'application/json',
			dataType : "json",
			type : "post",
			data : JSON.stringify(param),
			async : false,
			success : function(data) {
                $.baseMessageProgressClose();
				callback(data);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				handleResult(XMLHttpRequest);
			}
		});
	}

	$.baseGetNotAsync = function(url, callback) {
        $.baseMessageProgress({msg : '<spring:message code="global.progress.title"/>'})
		$.ajax({
			url : baseUrl + url,
			contentType : 'application/json',
			dataType : "json",
			type : "GET",
			async : false,
			success : function(data) {
                $.baseMessageProgressClose();
				callback(data);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				handleResult(XMLHttpRequest);
			}
		});
	}

	$.baseBindClick = function(elementId, func) {
		$(elementId).off('click').on('click', function(event){
			func($(this), event);
		})
	}

    $.baseBindChange = function(elementId, func) {
        $(elementId).off('change').on('change', function(event){
            func($(this), event);
        })
    }
	
	function handleResult(req) {
        $.baseMessageProgressClose();
		if(req && req.responseJSON) {
			var result = req.responseJSON;
			
			if(result.code == '00001') {

				$.baseErrorAlert(result.msg, function () {
                    window.location.href = baseUrl + "/login";
                });
			}else if(result.code == '00002') {
				$.baseErrorAlert(result.msg, function () {
                    window.location.reload();
                })
			}else if(result.code == '10001'){
				$.baseAlert(result.msg)
			}else if(result.code == '10002'){
                $.baseErrorAlert(result.msg)
            }
			else {
				//LoadAjaxContent(baseUrl + '/error/500');
			}
		}else{
			//LoadAjaxContent(baseUrl + '/error/500');
		}
		
	}
	
	
	$.baseStringToJson = function(data){
		return eval('(' + data + ')');;
	}
	
	$.getProjectName = function(){
		//获取项目名称
		var pathName = window.document.location.pathname;
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
		return projectName;
	}
	
	$.baseGetTimestamp = function(){
		return new Date().getTime();
	}

	$.baseGetNowFormatDate = function() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        return currentdate;
	}


})(jQuery);