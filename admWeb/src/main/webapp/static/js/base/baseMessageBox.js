(function($){

	var msgTitle = 'MESSAGE';

	$.baseAlert = function(msg, callback){
		$.messager.alert(msgTitle, msg, 'info', function(){
			if(callback){
				doCallback(callback);
			}
		})
	}

	$.baseWarningAlert = function(msg, callback){
		$.messager.alert(msgTitle, msg, 'warning', function(){
			doCallback(callback);
		})
	}

	$.baseQuestionAlert = function(msg, callback){
		$.messager.alert(msgTitle, msg, 'question', function(){
			doCallback(callback);
		})
	}

	$.baseErrorAlert = function(msg, callback){
		$.messager.alert(msgTitle, msg, 'error', function(){
			doCallback(callback);
		})
	}

	$.baseConfirm = function(msg, callback){
		$.messager.confirm(msgTitle, msg, function(r){
			if(r){
				doCallback(callback);
			}
		})
	}

	$.basePrompt = function(msg, callback){
        $.messager.prompt(msgTitle, msg, function(r){
            if (r){
                callback(r);
            }
        });
	}
	
	$.baseMessageShow = function(params){
		$.messager.show({
			title: params.title,
			msg: params.message,
			timeout: params.timeout ? params.timeout:5000,
			showType:'slide'
		});

	}

	$.baseMessageProgress = function(params){
        $.messager.progress({
            title:'Please waiting',
            msg: params.msg ? params.msg : 'LOADING......',
			text: params.text == undefined ? '' : params.text
        });
	}

	$.baseMessageProgressClose = function(){
        $.messager.progress('close');
	}

	$.baseDisabledButton = function(elementId){
		$('#' + elementId).linkbutton({disabled: true, text: 'processing'});
	}

	$.baseEnabledButton = function(elementId, btnText, showIconFlag){

		var iconCls = showIconFlag ? 'icon-save' : '';

		$('#' + elementId).linkbutton({disabled: false, text: btnText ? btnText:'SAVE', iconCls: iconCls});
	}

	function doCallback(callback) {
		if(callback){
			callback();
		}
	}
})(jQuery)