<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <title>天天陪练-机构管理系统</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <meta http-equiv="Cache-Control" content="no-store"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
    <sitemesh:head/>
    <link rel="shortcut icon" href="${ ctx }/static/images/favicon.ico">
    <link href="${ ctx }/static/css/default.css" rel="stylesheet" type="text/css"/>
    <link href="${ ctx }/static/css/framework.css" rel="stylesheet" type="text/css"/>
    <link href="${ ctx }/static/js/easy.ui/themes/gray/easyui.css" rel="stylesheet" type="text/css"/>
    <link href="${ ctx }/static/js/easy.ui/themes/icon.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="${ ctx }/static/js/easy.ui/jquery.js"></script>
    <%--<script type="text/javascript" src="${ ctx }/static/js/easy.ui/jquery.min.js"></script>--%>
    <script type="text/javascript" src="${ ctx }/static/js/easy.ui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ ctx }/static/js/easy.ui/locale/easyui-lang-${lang}.js"></script>
    <script type="text/javascript" src="${ ctx }/static/js/base/base-js.js"></script>
    <script type="text/javascript" src="${ ctx }/static/js/jquery.serializeJSON/jquery.serializeJSON.js"></script>
    <script type="text/javascript" src="${ ctx }/static/js/base/baseValidate.js"></script>
    <script type="text/javascript" src="${ ctx }/static/js/base/baseMessageBox.js"></script>
    <script type="text/javascript" src="${ ctx }/static/js/base/baseEasyui.js"></script>
    <script type="text/javascript" src="${ ctx }/static/js/base/default.js"></script>
    <script type="text/javascript" src="${ ctx }/static/js/template/template-web.js"></script>

    <script>
        !function (global) {
            global.baseUrl = '${ctx}';
            <%--global.amCode = '<shiro:principal property="amCode" />';--%>
            global.lang = '${lang}';

            //禁止页面右键
            $(document).bind("contextmenu",function(e){
                return false;
            });
        }(window);
    </script>
</head>
<body  class="easyui-layout" style="overflow: auto;">
<%@ include file="/WEB-INF/layouts/header.jsp" %>
<sitemesh:body/>
<%@ include file="/WEB-INF/layouts/footer.jsp" %>
</body>

</html>