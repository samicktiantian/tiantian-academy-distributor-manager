<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<body>
<div id="tb" style="padding: 10px;">
<form id="queryForm">
<spring:message code="role.name" /> ：<input name="roleName" class="easyui-textbox" placeholder="角色名称" value="">
    <a id="search" class="easyui-linkbutton"><spring:message code="global.button.search"/></a>
    <a id="add" class="easyui-linkbutton"><spring:message code="global.button.add"/></a>
</form>
</div>
<script>

    $(function(){

        initDataList();

        $.baseBindClick('#search', function(obj){
            var param = $("#queryForm").serializeJSON();
            datagrid.datagrid('load', param);
        })

        $.baseBindClick('#add', function(obj){
            $.baseDialog({
                url:'toAdd',
                title:'<spring:message code="global.list.addOrUpdate"/>',
                width: 400,
                height: 250,
                okFunction: function(obj){
                    doSubmit(obj);
                }
            });
        })

    })

    var datagrid;

    function initDataList(){

        datagrid = $.baseDatagrid({
            url : 'init',
            columns: [
                {field:'roleName',title:'<spring:message code="role.name" />',width:100, align: 'center'},
                {field:'description',title:'<spring:message code="role.descritpion" />',width:100, align: 'center'},
                {field:'action',title:'<spring:message code="global.button.action"/>',width:100,align:'center',
                    formatter:function(value,row,index){
                        var d = '';
                        if(row.id != '1') {
                            d = '<shiro:hasPermission name="role:delete"><a href="#" class="role-delete" data-idx="'+row.idx+'"><spring:message code="global.button.delete"/></a></shiro:hasPermission> | ';
                        }
                        var e = '<shiro:hasPermission name="role:auth"><a href="#" class="role-authc" data-idx="'+row.idx+'"><spring:message code="role.auth"/></a></shiro:hasPermission>';
                        return d+'&nbsp;'+e;
                    }
                }
            ],
            toolbarId:'#tb'	,
            dgId: 'dg' + $.baseGetTimestamp(),
            nowrap: false,
            fit: true,
            loadSuccess : function(){

                $.baseBindClick('.role-authc', function(obj){
                    var idx = $(obj).data('idx');
                    $.baseDialog({
                        url:'permissionList/'+idx,
                        title:'ROLE CONFIG',
                        width : 800,
                        height : 550,
                        okFunction: function(obj){
                            commit(function(){
                                $.closeDialog(obj);
                            });
                        }
                    });
                })

                $.baseBindClick('.role-delete', function(obj){
                    var idx = $(obj).data('idx');
                    $.baseConfirm('<spring:message code="global.list.delete.confirm"/>', function(){
                        $.baseGet('/role/delete/'+idx, function(data){
                            if(data.code == '10000'){
                                //保持更新前的页面信息
                                datagrid.datagrid('reload',$("#queryForm").serializeJSON());
                            }else{
                                $.baseErrorAlert(data.msg);
                            }
                        })
                    });
                })
            }
        });
    }

</script>
</body>