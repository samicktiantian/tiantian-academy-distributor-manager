<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>      
<body>
<style>
	.academy_table {
		width: 100%;
		min-height: 25px;
		line-height: 25px;
		border-collapse: collapse;
		border: 1px solid #0094ff;
	}

	.academy_table th, td {
		border: 1px solid #0094ff;
	}

	.academy_table thead {
		background-color: #00bbee;
		color: #fff;
		text-align: center
	}

	.column_1 {
		text-align: center
	}
</style>
<input id="roleIdx" type="hidden" value="${ roleIdx }" />
<table class="academy_table">
	<thead>
		<tr>
			<th style="width: 148px;">
				RESOURCE
			</th>
			<th style="width: 157px;" colspan="2">
				PERMISSION
			</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${ rolePermissionList }" var="resource">
		<tr>
			<td class="column_1">
				${ resource.name }
			</td>
			<td colspan="2">
				<c:forEach items="${ resource.permissionList }" var="permission">
					<label class=""><input name="permssionIdx" <c:if test="${ permission.permitFlag == 1 }">checked="checked"</c:if> type="checkbox" value="${ permission.idx }"> ${ permission.name }
					</label>
				</c:forEach>
			</td>
		</tr>
	</c:forEach>
	</tbody>
</table>
<script>


function commit(callback){


	var permissionIds = new Array();
	$('input[name="permssionIdx"]:checked').each(function () {
	    permissionIds.push(this.value);
	});
	
	var rolePermissionData = {
		permissionIds : permissionIds,
		idx : $('#roleIdx').val()
	}

    $.baseDisabledButton('save-btn')

	$.basePost('/role/setRoleResourcePermission', rolePermissionData, function(data) {

        $.baseEnabledButton('save-btn', 'SAVE');

		if(data.code=='10000'){
			$.baseAlert('SUCCESS！');
			callback();
		}else{
			$.baseErrorAlert('FAIL！');
		}
	});
	
}
</script>
</body>
