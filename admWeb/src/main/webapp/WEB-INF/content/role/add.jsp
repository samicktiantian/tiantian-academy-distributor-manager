<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="st" uri="/WEB-INF/tag/selectTag.tld" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<body>
	<div style="padding:20px 60px 20px 60px">
		<form id="dataForm" method="post" class="easyui-form">
	    	<table cellpadding="4">
	    		<tr>
	    			<td><spring:message code="role.name" />:</td>
	    			<td><input type="text" class="easyui-textbox easyui-validatebox" name="roleName"  data-options="required:true" /></td>
	    		</tr>
				<tr>
					<td><spring:message code="role.identity"/>:</td>
					<td><input type="text" class="easyui-textbox easyui-validatebox" name="identity" data-options="required:true" /></td>
				</tr>
	    		<tr>
	    			<td><spring:message code="role.descritpion" />:</td>
	    			<td><input type="text" class="easyui-textbox easyui-validatebox" name="description" /></td>
	    		</tr>
	    	</table>
	    </form>
	</div>
	<script type="text/javascript">

	function doSubmit(obj){
		$.baseFormValidate({
			formId: 'dataForm',
			url: 'add',
			disabledBtnId : 'save-btn',
			success: function(){
				$.baseAlert('SUCCESS！');
				//返回成功
				$.closeDialog(obj);
	        	datagrid.datagrid('load',{});
			}
		});
	} 
	</script>
</body>
</html>