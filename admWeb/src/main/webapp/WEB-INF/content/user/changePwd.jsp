<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<body>
<div style="padding:10px 60px 20px 60px">
    <form id="dataForm" method="post" class="easyui-form">
    <table cellpadding="5">
   		<tr>
   			<td>原始密码:</td>
   			<td><input class="easyui-textbox" type="password" name="oldPassword" data-options="required:true" /></td>
   		</tr>
   		<tr>
   			<td>新密码:</td>
   			<td><input class="easyui-textbox" id="password" type="password" name="password" data-options="required:true" /></td>
   		</tr>
   		<tr>
   			<td>重复密码:</td>
   			<td><input class="easyui-textbox" type="password" name="rpassword" data-options="required:true,validType:'same[\'password\']'" /></td>
   		</tr>
   	</table>
</form>
</div>
<script type="text/javascript">
console.log(2222222);
function updatePasswd(obj){
    $.baseFormValidate({
        formId: 'dataForm',
        url: baseUrl + '/user/modifyPassword',
        disabledBtnId : 'save-btn',
        success: function(){
            $.baseAlert('SUCCESS！');
            //返回成功
            $.closeDialog(obj);
        }
    });
}
</script>
</body>
