<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div id="tb" style="padding: 10px;">
<form id="queryForm">
<spring:message code="user.loginid"/>: <input class="easyui-textbox" name="loginName" placeholder="login name" value="">
<spring:message code="user.realname"/>：<input class="easyui-textbox" name="realName" placeholder="real name">
<spring:message code="user.mobile"/>：<input class="easyui-textbox" name="mobile" placeholder="mobile">
<spring:message code="user.email"/>：<input class="easyui-textbox" name="email" placeholder="email">
<a id="search" class="easyui-linkbutton" ><spring:message code="global.button.search"/></a>
    <shiro:hasPermission name="user:add"><a id="add" class="easyui-linkbutton" ><spring:message code="global.button.add"/></a></shiro:hasPermission>
</form>
</div>
<script>

    $(function(){

        initDataList();

        $.baseBindClick('#search', function(obj){
            var param = $("#queryForm").serializeJSON();
            datagrid.datagrid('load', param);
        })

        $.baseBindClick('#add', function(obj){
            $.baseDialog({
                url:'toAdd',
                title:'<spring:message code="global.list.addOrUpdate"/>',
                height: 520,
                okFunction: function(obj){
                    doSubmit(obj);
                }
            });
        })
    })


var datagrid;

function initDataList(){
    datagrid = $.baseDatagrid({
        url : 'init',
        columns: [
            {field:'loginName',title:'<spring:message code="user.loginid"/>',width:100, align: 'center'},
            {field:'realName',title:'<spring:message code="user.realname"/>',width:100, align: 'center'},
            {field:'email',title:'<spring:message code="user.email"/>',width:200, align: 'center'},
            {field:'mobile',title:'<spring:message code="user.mobile"/>',width:100, align: 'center'},
            {field:'use',title:'<spring:message code="user.isUse"/>',width:100, align: 'center'},
            {field:'language',title:'<spring:message code="user.language"/>',width:100, align: 'center'},
            {field:'action',title:'<spring:message code="global.button.action"/>',width:100,align:'center',
                formatter:function(value,row,index){
                    var e = '<shiro:hasPermission name="user:update"><a href="#" class="user-edit" data-idx="'+row.idx+'"><spring:message code="global.button.update"/></a></shiro:hasPermission>';
                    var d = '<shiro:hasPermission name="user:delete"> | <a href="#" class="user-del" data-idx="'+row.idx+'"><spring:message code="global.button.delete"/></a></shiro:hasPermission>';
                    return e+d;
                }
            }
        ],
        toolbarId:'#tb'	,
        dgId: 'dg' + $.baseGetTimestamp(),
        fit: true,
        loadSuccess : function(){

            $.baseBindClick('.user-edit', function(obj){
                $.baseDialog({
                    url:'view/'+$(obj).data('idx'),
                    title:'<spring:message code="global.list.addOrUpdate"/>',
                    height: 520,
                    okFunction: function(obj){
                        doSubmit(obj, datagrid);
                    }
                });
            })

            $.baseBindClick('.user-del', function(obj){
                var idx = $(obj).data('idx');
                $.baseConfirm('<spring:message code="global.list.delete.confirm"/>', function(){
                    $.baseGet('/user/del/'+idx, function(data){
                        if(data.code == '10000'){
                            //保持更新前的页面信息
                            datagrid.datagrid('reload',$("#queryForm").serializeJSON());
                        }else{
                            $.baseErrorAlert(data.msg);
                        }
                    })
                });
            })
        }
    });
}



</script>
