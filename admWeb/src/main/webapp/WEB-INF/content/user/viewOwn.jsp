<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<div style="padding:10px 60px 20px 60px">
		<form id="dataForm" method="post" class="easyui-form">
			<input type="hidden" name="idx" value="${ user.idx }">

	    	<table cellpadding="4">
	    		<tr>
	    			<td><spring:message code="user.loginid"/>:</td>
	    			<td><input type="text" class="easyui-textbox easyui-validatebox" name="loginName" <c:if test="${ !empty user.idx }">readonly</c:if> <c:if test="${ empty user.idx }"> data-options="required:true" </c:if> value="${ user.loginName }"></input></td>
	    		</tr>
	    		<tr>
	    			<td><spring:message code="user.realname"/>:</td>
	    			<td><input type="text" class="easyui-textbox easyui-validatebox" name="realName" data-options="required:true" value="${ user.realName }"></input></td>
	    		</tr>
	    		<tr>
	    			<td><spring:message code="user.email"/>:</td>
	    			<td><input type="text" class="easyui-textbox easyui-validatebox" name="email" data-options="validType:'email'" value="${ user.email }"></input></td>
	    		</tr>
	    		<tr>
	    			<td><spring:message code="user.mobile"/>:</td>
	    			<td><input name="mobile" class="easyui-textbox easyui-validatebox" data-options="validType:'mobile'" value="${ user.mobile }"></input></td>
	    		</tr>
				<tr>
					<td><spring:message code="user.language"/>:</td>
					<td>
						<select id="language" name="language" style="width:143px;" class="easyui-combobox easyui-validatebox" data-options="required:true">
								<option <c:if test="${ user.language == 'zh' }">selected</c:if> value="zh">中文</option>
								<option <c:if test="${ user.language == 'ko_kr' }">selected</c:if> value="ko_kr">한국어</option>
								<option <c:if test="${ user.language == 'en' }">selected</c:if> value="en">English</option>
						</select>
					</td>
				</tr>
	    	</table>
	    </form>
	</div>
	<script type="text/javascript">

	function doSubmit(obj){
		$.baseFormValidate({
			formId: 'dataForm',
			url: baseUrl + '/user/updateOwn',
			disabledBtnId : 'save-btn',
			success: function(){
				$.baseAlert('SUCCESS！');
				//返回成功
				$.closeDialog(obj);

				if(datagrid) {
                    datagrid.datagrid('load',{});
				}
			}
		});
	}

	</script>
