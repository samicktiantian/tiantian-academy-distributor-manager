<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div id="tb" style="padding: 10px;">
<form id="queryForm">
<spring:message code="code.dist"/>: <input class="easyui-textbox" name="cdDist" placeholder="<spring:message code="code.dist"/>" value="">
<spring:message code="code.code"/>: <input class="easyui-textbox" name="cdCode" placeholder="<spring:message code="code.code"/>" value="">

<a id="search" class="easyui-linkbutton" ><spring:message code="global.button.search"/></a>
<%--<shiro:hasPermission name="code:add"><a id="add" class="easyui-linkbutton" ><spring:message code="global.button.add"/></a></shiro:hasPermission>--%>
</form>
</div>
<script>

    $(function(){
        initDataList();

        $.baseBindClick('#search', function(obj){
            var param = $("#queryForm").serializeJSON();
            datagrid.datagrid('load', param);
        })

    })

    var datagrid;

    function initDataList(){
        datagrid = $.baseDatagrid({
            url : 'init',
            columns: [
                {field:'cdDist',title:'<spring:message code="code.dist"/>',width:100, align: 'center'},
                {field:'cdCode',title:'<spring:message code="code.code"/>',width:100, align: 'center'},
                {field:'cdNameKo',title:'<spring:message code="code.name.ko"/>',width:100, align: 'center'},
                {field:'cdNameZhCn',title:'<spring:message code="code.name.zh"/>',width:100, align: 'center'},
                {field:'cdNameEn',title:'<spring:message code="code.name.en"/>',width:100, align: 'center'},
                {field:'cdLevel',title:'<spring:message code="code.level"/>',width:100, align: 'center'},
                {field:'use',title:'<spring:message code="entity.isUse"/>',width:50, align: 'center'},
                {field:'action',title:'<spring:message code="global.button.action"/>',width:100,align:'center',
                    formatter:function(value,row,index){
                        var e = '<shiro:hasPermission name="code:update"><a href="#" class="edit-btn" data-dist="'+row.cdDist+'" data-code="'+row.cdCode+'" "><spring:message code="global.button.update"/></a></shiro:hasPermission>';
                        var d = '<shiro:hasPermission name="code:delete"> | <a href="#" class="del-btn" ><spring:message code="global.button.delete"/></a></shiro:hasPermission>';
                        return e/*+d*/;
                    }
                }
            ],
            toolbarId:'#tb'	,
            dgId: 'dg' + $.baseGetTimestamp(),
            fit: true,
            loadSuccess : function(){

                $.baseBindClick('.edit-btn', function(obj){
                    var dist = $(obj).data('dist');
                    var code = $(obj).data('code');

                    $.baseDialog({
                        url:'view/'+dist+'/'+code,
                        title:'<spring:message code="global.list.addOrUpdate"/>',
                        height: 400,
                        okFunction: function(obj){
                            doSubmit(obj);
                        }
                    });
                })

            }
        });
    }


</script>
