<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="st" uri="/WEB-INF/tag/selectTag.tld" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<body>
	<div style="padding:20px 60px 20px 60px">
		<form id="dataForm" method="post" class="easyui-form">
	    	<table cellpadding="4">
	    		<tr>
	    			<td><spring:message code="code.dist" />:</td>
	    			<td><input type="text" class="easyui-validatebox" name="cdDist" readonly value="${ data.cdDist}"/></td>
	    		</tr>
				<tr>
					<td><spring:message code="code.code" />:</td>
					<td><input type="text" class="easyui-validatebox" name="cdCode" readonly value="${ data.cdCode}"/></td>
				</tr>
				<tr>
					<td><spring:message code="code.name.ko" />:</td>
					<td><input type="text" class="easyui-validatebox" name="cdNameKo" value="${ data.cdNameKo}"/></td>
				</tr>
				<tr>
					<td><spring:message code="code.name.zh" />:</td>
					<td><input type="text" class="easyui-validatebox" name="cdNameZhCn" value="${ data.cdNameZhCn}"/></td>
				</tr>
				<tr>
					<td><spring:message code="code.name.en" />:</td>
					<td><input type="text" class="easyui-validatebox" name="cdNameEn" value="${ data.cdNameEn}"/></td>
				</tr>
				<tr>
					<td><spring:message code="code.level" />:</td>
					<td><input type="text" class="easyui-validatebox" name="cdLevel" value="${ data.cdLevel}"/></td>
				</tr>
				<tr>
					<td><spring:message code="entity.isUse" />:</td>
					<td>
						<select id="use" name="use" style="width:143px;" class="easyui-combobox easyui-validatebox" data-options="required:true">
							<option <c:if test="${ data.use == 'Y' }">selected</c:if> value="Y">YES</option>
							<option <c:if test="${ data.use == 'N' }">selected</c:if> value="N">NO</option>
						</select>
					</td>
				</tr>
	    	</table>
	    </form>
	</div>
	<script type="text/javascript">

	function doSubmit(obj){
		$.baseFormValidate({
			formId: 'dataForm',
			url: 'add',
			disabledBtnId : 'save-btn',
			success: function(){
				$.baseAlert('SUCCESS！');
				//返回成功
				$.closeDialog(obj);
	        	datagrid.datagrid('load',{});
			}
		});
	} 
	</script>
</body>
</html>