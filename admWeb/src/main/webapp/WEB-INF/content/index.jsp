<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script>
    !$(function(){
        initLeftMenu();
        tabClose();
        tabCloseEven();
    })
</script>
<div region="north" split="true" border="false" style="overflow: hidden; height: 30px;
        background: #7f99be repeat-x center 50%;
        line-height: 20px;color: #fff; font-family: Verdana, 微软雅黑,黑体">
        <span style="float:right; padding-right:20px;" class="head">
            <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm2'">
                <shiro:principal property="userName"/>
            </a>
            <div id="mm2" class="easyui-menu" style="width:120px;">
                <div data-options="iconCls:'icon-man'"><a href="javascript:javascript:void(0);" id="update-am-info"><spring:message code="user.btn.updateAmInfo" /></a></div>
                <%--<div data-options="iconCls:'icon-man'"><a href="javascript:javascript:void(0);" id="update-info"><spring:message code="user.btn.updateinfo" /></a></div>--%>
                <div data-options="iconCls:'icon-man'"><a href="javascript:javascript:void(0);" id="change-pwd"><spring:message code="user.btn.changepwd" /></a></div>
                <div data-options="iconCls:'icon-help'"><a href="${ctx}/academyMaster/qrCode" target="_blank"><spring:message code="academy.qrcode" /></a></div>
                <div class="menu-sep"></div>
                <div><a href="logout" id="loginOut">LOGOUT</a></div>
            </div>
        </span>
    <span style="padding-left:10px; font-size: 16px; ">后台管理系统</span>
</div>
<div region="south" split="true" style="height: 30px; background: #D2E0F2; ">
    <div class="footer">欢迎使用后台管理系统</div>
</div>
<div region="west" hide="true" split="true" title="Navigation menu" style="width:220px;" id="west">
    <div id="nav" class="easyui-accordion" fit="true" border="false">
        <!--  导航内容 -->

    </div>

</div>
<div id="mainPanle" region="center" style="background: #eee; ">
    <div id="tabs" class="easyui-tabs" fit="true" border="false" style="overflow-y: visible;height: 99.5%;">
    </div>
</div>

<div id="mm" class="easyui-menu" style="width:150px; display: none;">
    <div id="mm-tabupdate">刷新</div>
    <div class="menu-sep"></div>
    <div id="mm-tabclose">关闭</div>
    <div id="mm-tabcloseall">全部关闭</div>
    <div id="mm-tabcloseother">除此之外全部关闭</div>
    <div class="menu-sep"></div>
    <div id="mm-tabcloseright">当前页右侧全部关闭</div>
    <div id="mm-tabcloseleft">当前页左侧全部关闭</div>
    <div class="menu-sep"></div>
    <div id="mm-exit">退出</div>
</div>

<script>
    console.log(1111111);
    $(function(){

        $.baseBindClick('#change-pwd', function(obj){
            $.baseDialog({
                url: baseUrl + '/user/toChangePwdpage',
                title:'<spring:message code="user.btn.changepwd"/>',
                height: 240,
                okFunction: function(obj){
                    updatePasswd(obj);
                }
            });
        })

        $.baseBindClick('#update-am-info', function(obj){
            if(amCode && amCode != 'null') {
                $.baseDialog({
                    url: baseUrl + '/academyMaster/view/'+ amCode,
                    title:'<spring:message code="global.list.addOrUpdate"/>',
                    height: 570,
                    okFunction: function(obj){
                        doSubmit(obj);
                    }
                });
            }
        })

        $.baseBindClick('#update-info', function(obj){
            $.baseDialog({
                url: baseUrl + '/user/viewOwn',
                title:'<spring:message code="global.list.addOrUpdate"/>',
                height: 420,
                okFunction: function(obj){
                    doSubmit(obj);
                }
            });
        })
    })
</script>
