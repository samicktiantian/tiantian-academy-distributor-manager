<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>天天陪练-机构管理系统</title>
    <link rel="shortcut icon" href="${ ctx }/static/images/favicon.ico">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="${ctx}/static/login/css/login.css?ver=5">
</head>
<body>

<div class="login-d">
    <div class="login-form">
        <form id="login-form" class="loginForm" method="post">
            <div class="login-title">
                <span>欢迎使用</span> 天天陪练销售管理后台
            </div>
            <div class="login-i">
                <input type="text" placeholder="请输入登陆账号" id="loginName" name="loginName">
            </div>

            <div class="login-i">
                <input type="password" placeholder="请输入登陆密码" id="password" name="password">
            </div>

            <div class="login-i pull-left">
                <input type="text" class="code-i" placeholder="请输入验证码" id="validCode" name="validCode">
                <span class="valid-code">
                    <a id="arandom" name="arandom" href="javascript:changeImg()">
                        <img id="imgrandom" name="imgrandom" src="randomNo">
                    </a>
                </span>
            </div>

            <div class="login-i">
                <input type="button" class="login-btn" id="login-btn" />
            </div>

            <div id="errorContent"></div>
        </form>
    </div>


</div>


<%--<h1>
    天天陪练-机构后台管理系统<sup>beta v1.0</sup>
</h1>
<div class="login">
    <div class="header">
        <div class="switch" id="switch">
            <div class="switch_bottom" id="switch_bottom" style="position: absolute; width: 64px; left: 0px;"></div>
        </div>
    </div>
    <div class="web_qr_login" id="web_qr_login" style="display: block; height: 290px;">
        <!--登录-->
        <div class="web_login" id="web_login">
            <div class="login-box">
                <div class="login_form">
                    <form id="login-form" class="loginForm" method="post">
                        <input type="hidden" name="to" value="log"/>
                        <div class="uinArea">
                            <label class="input-tips" for="loginName">LOGIN ID：</label>
                            <div class="inputOuter" id="uArea">
                                <input type="text" id="loginName" name="loginName"
                                       class="inputstyle" value=""/>
                            </div>
                        </div>
                        <div class="pwdArea" id="pwdArea">
                            <label class="input-tips" for="password">PASSWORD：</label>
                            <div class="inputOuter">
                                <input type="password" id="password" name="password"
                                       class="inputstyle" value=""/>
                            </div>
                        </div>
                        <div class="pwdArea">
                            <label class="input-tips" for="validCode">CODE：</label>
                            <div class="inputOuter">
                                <input type="text" id="validCode" name="validCode"
                                       class="validatestyle"/>
                                <span class="imgRandom">
										<a id="arandom" name="arandom" href="javascript:changeImg()">
											<img id="imgrandom" name="imgrandom" src="randomNo">
										</a>
									</span>
                            </div>
                        </div>
                        <div id="errorContent"></div>
                        <div style="padding-left: 50px; margin-top: 20px;">
                            <input type="submit" value="SIGN IN" style="width: 150px;"
                                   class="button_blue"/>
                        </div>
                    </form>
                </div>

            </div>

        </div>
        <!--登录end-->
    </div>
</div>--%>
<script src="${ctx }/static/js/easy.ui/jquery.min.js" type="text/javascript"></script>
<script src="${ctx }/static/js/validate/jquery.validate.min.js"></script>
<script>
    (function () {

        if (window.frames.length != parent.frames.length) {
            parent.location.href = document.location.href;
        }

        $('#login-btn').on('click', function(){
            submit();
        })

        $('#loginName').focus();

        $('#login-form input').keypress(function (e) {
            if (e.which == 13) {
                submit();
            }
        });

    }());

    function submit(){
        var param = {
            "loginName": $.trim($('#loginName').val()),
            "password": $.trim($('#password').val()),
            "validCode": $.trim($('#validCode').val())
        }
        if(param.loginName == ''){
            $('#errorContent').html('请输入账号');
            return;
        }
        if(param.password == ''){
            $('#errorContent').html('请输入密码');
            return;
        }
        if(param.validCode== ''){
            $('#errorContent').html('请输入验证码');
            return;
        }

        $.ajax({
            url: '${ctx}/webLogin?timezone=' + new Date().getTimezoneOffset(),
            contentType: 'application/json;charset=utf-8',
            dataType: "json",
            type: "post",
            data: JSON.stringify(param),
            success: function (data) {
                if (data && data.code == '10000') {
                    window.location.href = '${ctx}/index';
                } else {
                    changeImg();
                    $('#errorContent').html(data.msg);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#errorContent').html(XMLHttpRequest.responseJSON.msg);
            }
        });
    }

    function changeImg() {
        $("#imgrandom").attr("src", "randomNo?" + getURLTimeStamp());
    }

    function getURLTimeStamp() {
        var dt = new Date();
        return "ts=" + dt.getTime();
    }
</script>
</body>
</html>