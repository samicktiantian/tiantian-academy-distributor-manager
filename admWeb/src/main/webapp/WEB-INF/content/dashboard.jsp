<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<body >
<link rel="stylesheet" href="${ctx}/static/css/academy-table.css?ver=1">
<div style="margin: 10px;">
    欢迎使用
</div>
<script>
</script>

<script id="data-today-stratistic-total" type="text/html">
{{each list n index}}
<tr>
    <td class="table_text_only">{{n.sbsType[lang]}}</td>
    <td class="table_text_only">{{n.ccType[lang]}}</td>
    <td class="table_text_only">{{n.ccDuration}}</td>
    <td class="table_text_only">{{n.assignSuccessCount}}</td>
    <td class="table_text_only">{{n.resSuccessCount}}</td>
    <td class="table_text_only">{{n.resCancelCount}}</td>
    <td class="table_text_only">{{n.resFinishCount}}</td>
</tr>
{{/each}}
</script>

</body>

